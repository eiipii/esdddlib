/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.test

import com.eiipii.esddd.eventsourcing.api.{CommandDomainError, DomainInstance, EventCommandExecution, FinalCommandToEventsResult}
import com.eiipii.esddd.eventsourcing.eventstore.EventStoreInteraction
import com.eiipii.esddd.eventsourcing.logic.{DomainCoreLogic, FinalCommandExecutionResult}
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainErrorCode}
import com.eiipii.esddd.eventsourcing.projections.EventsProjection
import org.scalatest.exceptions.TestFailedException

import scalaz._

class DomainTestPositiveTests extends MockedDomainTest
    with StandardCommandHistoryTest[TestLogicDomainSpecification, TheState] {
  override def commandsShouldSuccess: Boolean = true
}

class DomainTestCornerCasesNegative extends MockedDomainTest {

  override def commandsShouldSuccess: Boolean = false

  it should "assert and error on tests when a command do not execute correctly" in {
    val lineNumber = 47
    try {
      forAll(genCommandExecutionHistory()) { history =>
        if (history.commandsAndResults.nonEmpty) {
          fail("No error assertion during command execution")
        }
      }
    }
    catch {
      case e: TestFailedException =>
        e.message match {
          case Some(m) => m should include("Generated commands must run without errors, the command ")
          case None    => fail("Command execution did produce the desired test error", e)
        }
        e.failedCodeFileNameAndLineNumberString match {
          case Some(s) => s should equal(s"MockedDomainTest.scala:${lineNumber + 2}")
          case None    => fail("Command execution did produce the desired test error", e)
        }
      case e: Throwable =>
        fail("Command execution did produce the desired test error", e)
    }
  }

  override def validation: DomainCoreModelValidation[TestLogicDomainSpecification, TheState] = new DomainCoreModelValidation[TestLogicDomainSpecification, TheState] {
    override def postCommandValidation(command: TheCommand, rootAggregate: TheAggregate, state: TheState, projection: TheState): Unit = {
      fail("always fail on post command validation")
    }

    override def validateState(state: TheState, projection: TheState): Unit = {
      fail("always fail on state validation")
    }
  }
}

/** A standard domain test with mocked implementation. This test that the tests are executed by coverage analysis.
 */
abstract class MockedDomainTest extends AbstractDomainCoreTest[TestLogicDomainSpecification, TheState] {

  def commandsShouldSuccess: Boolean

  override def domainInstanceFromLogic[T <: DomainCoreModel](domainLogic: DomainCoreLogic[T]): DomainInstance[T] =
    new DomainInstance[TestLogicDomainSpecification] {
      override def commandExecution: EventCommandExecution[TestLogicDomainSpecification] = new FakeDomainApi(commandsShouldSuccess)

      override def eventStore: EventStoreInteraction[TestLogicDomainSpecification] = Mocked.shouldNotBeCalled
    }.asInstanceOf[DomainInstance[T]]

  //Login to test
  override def logic(): DomainCoreLogic[TestLogicDomainSpecification] = new TestDomainLogic()

  override def projection(): EventsProjection[TestLogicDomainSpecification, TheState] =
    new EventsProjection[TestLogicDomainSpecification, TheState] {
      override def zero(): TheState = SuccessState()

      override def projectSingleEvent(state: TheState, event: TheEvent): TheState = SuccessState()
    }

  override def commandGenerator(): DomainCoreCommandGenerator[TestLogicDomainSpecification, TheState] = new TheCommandGenerator()

  override def validation: DomainCoreModelValidation[TestLogicDomainSpecification, TheState] = new DomainCoreModelValidation[TestLogicDomainSpecification, TheState] {
    override def postCommandValidation(command: TheCommand, rootAggregate: TheAggregate, state: TheState, projection: TheState): Unit = state.ok should be(true)

    override def validateState(state: TheState, projection: TheState): Unit = state.ok should be(true)
  }
}

class FakeDomainApi[D <: DomainCoreModel](val commandsShouldSuccess: Boolean) extends EventCommandExecution[D] {
  lazy val fakeState: D#State = new TheState {
    override def ok: Boolean = true
  }.asInstanceOf[D#State]

  lazy val fakeAggregate: D#Aggregate = AggregateOne().asInstanceOf[D#Aggregate]

  override def execute(command: D#Command): FinalCommandToEventsResult[D] =
    if (commandsShouldSuccess) {
      \/-(FinalCommandExecutionResult(fakeAggregate, fakeState, Seq()))
    }
    else {
      -\/(CommandDomainError(DomainErrorCode(-1)))
    }

}