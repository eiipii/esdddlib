/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.test

import com.eiipii.esddd.eventsourcing.logic.FinalCommandExecutionResult
import org.scalacheck.Shrink
import org.scalatest.{FlatSpec, Matchers}

import scala.language.reflectiveCalls

class CommandExecutionHistoryShrinkTest extends FlatSpec with Matchers {

  it should "reduce execution history by one element" in {
    val history: CommandExecutionHistory[TestLogicDomainSpecification, TheState] = CommandExecutionHistory[TestLogicDomainSpecification, TheState](
      List.fill(4) {
        SingleCommandExecution[TestLogicDomainSpecification, TheState](
          CommandOne(),
          FinalCommandExecutionResult[TestLogicDomainSpecification](AggregateOne(), SuccessState(), Seq()),
          SuccessState()
        )
      }
    )
    val shrinkTest = new CommandExecutionHistoryShrink[TestLogicDomainSpecification, TheState] {
      def executeTest(): List[CommandExecutionHistory[TestLogicDomainSpecification, TheState]] = Shrink.shrink(history).toList
    }
    val result = shrinkTest.executeTest()
    result.size shouldBe 1
    result.head.commandsAndResults.size shouldBe 3
  }

  it should "Finish when no more data is available" in {
    val history: CommandExecutionHistory[TestLogicDomainSpecification, TheState] = CommandExecutionHistory[TestLogicDomainSpecification, TheState](List())

    val shrinkTest = new CommandExecutionHistoryShrink[TestLogicDomainSpecification, TheState] {
      def executeTest(): List[CommandExecutionHistory[TestLogicDomainSpecification, TheState]] = Shrink.shrink(history).toList
    }
    shrinkTest.executeTest() shouldBe empty
  }

}
