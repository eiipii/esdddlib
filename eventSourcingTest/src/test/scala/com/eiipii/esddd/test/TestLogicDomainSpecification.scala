/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.test

import com.eiipii.esddd.eventsourcing._
import com.eiipii.esddd.eventsourcing.logic._
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}
import org.scalacheck.Gen

import scalaz.\/-

class TestLogicDomainSpecification extends DomainCoreModel {
  type Command = TheCommand
  type Event = TheEvent
  type Aggregate = TheAggregate
  type ConstraintScope = TheConstraintScope
  type State = TheState
}

class TestDomainLogic extends DomainCoreLogic[TestLogicDomainSpecification] {

  override def calculateAggregates(command: TheCommand, state: TheState): DomainCommandToAggregateScope[TestLogicDomainSpecification] =
    command match {
      case CommandOne() => \/-(Set(AggregateOne()))
      case CommandTwo() => \/-(Set(AggregateTwo()))
    }

  override def calculateConstraints(command: TheCommand, state: TheState): DomainCommandToConstraints[TestLogicDomainSpecification] =
    command match {
      case CommandOne() => \/-(Set(ConstraintScopeOne()))
      case CommandTwo() => \/-(Set(ConstraintScopeTwo()))
    }

  override def calculateCommandToEvents(command: TheCommand)(implicit state: TheState): DomainCommandToEventsResult[TestLogicDomainSpecification] =
    command match {
      case CommandOne() => \/-(CommandExecutionResult(List(EventOne()), AggregateOne()))
      case CommandTwo() => \/-(CommandExecutionResult(List(EventTwo()), AggregateTwo()))
    }

  override def atomicProjectionProjection: EventStoreAtomicProjectionPoint[TestLogicDomainSpecification] =
    new EventStoreAtomicProjectionPoint[TestLogicDomainSpecification] {
      override def zero: EventStoreAtomicProjectionPoint[TestLogicDomainSpecification] = this

      override def state: TheState = SuccessState()

      override def projectEvent(event: TheEvent): EventStoreAtomicProjectionPoint[TestLogicDomainSpecification] = this
    }

  override def equalityRelations: DomainEqualityRelations[TestLogicDomainSpecification] =
    new DomainEqualityRelations[TestLogicDomainSpecification] {
      override def buildAggregateReference(aggregate: TheAggregate): AggregateReference = aggregate match {
        case AggregateOne() => AggregateReference(1, 0L)
        case AggregateTwo() => AggregateReference(2, 0L)
      }

      override def buildConstraintReference(constraintScope: TheConstraintScope): Option[ConstraintReference] = None
    }
}

sealed trait TheCommand

case class CommandOne() extends TheCommand

case class CommandTwo() extends TheCommand

sealed trait TheEvent

case class EventOne() extends TheEvent

case class EventTwo() extends TheEvent

case class EventWithData(nr: Long) extends TheEvent

sealed trait TheAggregate

case class AggregateOne() extends TheAggregate

case class AggregateTwo() extends TheAggregate

sealed trait TheConstraintScope

case class ConstraintScopeOne() extends TheConstraintScope

case class ConstraintScopeTwo() extends TheConstraintScope

trait TheState {
  def ok: Boolean
}

case class SuccessState() extends TheState {
  override def ok: Boolean = true
}

case class FailureState() extends TheState {
  override def ok: Boolean = false
}

class TheCommandGenerator extends DomainCoreCommandGenerator[TestLogicDomainSpecification, TheState] {
  lazy val genOneOrTwo = Gen.oneOf(genOne, genTwo)
  lazy val genOne = Gen.const(CommandOne())
  lazy val genTwo = Gen.const(CommandTwo())

  override def genCommand(state: TheState, projection: TheState): Gen[TheCommand] = genOneOrTwo
}
