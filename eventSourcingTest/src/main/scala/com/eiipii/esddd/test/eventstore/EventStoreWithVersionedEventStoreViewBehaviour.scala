/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.test.eventstore

import java.util.concurrent._
import java.util.concurrent.atomic.{AtomicBoolean, AtomicReference}

import com.eiipii.esddd.eventsourcing.{EventStoreRange, EventStoreVersion}
import com.eiipii.esddd.eventsourcing.eventstore._
import com.eiipii.esddd.test.ConcurrencyTest
import com.eiipii.esddd.test.model._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.PropertyChecks
import org.scalatest.{AppendedClues, FlatSpec, Matchers, Tag}
import org.typelevel.scalatest.DisjunctionMatchers

import scalaz._

/** Test behaviour of a event store implementation
 */
// $COVERAGE-OFF$
trait EventStoreWithVersionedEventStoreViewBehaviour {
  self: FlatSpec with Matchers with PropertyChecks with ScalaFutures with DisjunctionMatchers with AppendedClues =>

  def eventStoreWithAtomicProjection(
    testsTag:                     Tag,
    eventStoreCreator:            () => EventStore[TheTestDomainModel],
    isolationLevelIsSerializable: Boolean
  ): Unit = {

    it should "load events from backend" taggedAs testsTag in {
      //given
      val eventStore = eventStoreCreator()
      //and the event store is empty
      eventStore.loadCurrentVersion() shouldBe (EventStoreVersion.zero)
      //when events are added
      val addFunc: (Int) => Unit = addOneEvent(eventStore)
      addFunc(1)
      addFunc(2)
      addFunc(3)

      //then
      eventStore.loadEvents(EventStoreRange(EventStoreVersion.zero)) shouldBe (List(TestEventThread(0, 1, 0), TestEventThread(0, 2, 0), TestEventThread(0, 3, 0)))
      eventStore.loadCurrentVersion() shouldBe (EventStoreVersion(3))
      eventStore.loadEventsForAggregate(TestAggregateThread(0), EventStoreRange(EventStoreVersion.zero), 3).events.toVector shouldBe (List())
      eventStore.loadEventsForAggregate(TestAggregateThread(1), EventStoreRange(EventStoreVersion.zero), 3).events.toVector shouldBe (List(EventReference[TheTestDomainModel](TestEventThread(0, 1, 0), EventStoreVersion(1))))
      eventStore.loadEventsForAggregate(TestAggregateThread(2), EventStoreRange(EventStoreVersion.zero), 3).events.toVector shouldBe (List(EventReference[TheTestDomainModel](TestEventThread(0, 2, 0), EventStoreVersion(2))))
      eventStore.loadEventsForAggregate(TestAggregateThread(3), EventStoreRange(EventStoreVersion.zero), 3).events.toVector shouldBe (List(EventReference[TheTestDomainModel](TestEventThread(0, 3, 0), EventStoreVersion(3))))
    }
    it should "load events from aggregate in correct ranges" taggedAs testsTag in {
      //given
      val eventStore = eventStoreCreator()

      val nrOfEvents = 10
      //when events are added
      (1 to nrOfEvents).foreach { nr =>
        addOneEventOnThreadRoot(eventStore)(nr, 1)
        addOneEventOnThreadRoot(eventStore)(nr, 2) //add events on other aggregate
      }

      //then
      val expectedEvents = (1 to nrOfEvents).map(nr => List(TestEventThread(nr, 1, 0), TestEventThread(nr, 2, 0))).flatten
      eventStore.loadEvents(EventStoreRange(EventStoreVersion.zero)) shouldBe expectedEvents
      val possibleRanges = for {
        from <- (0 to 2 * nrOfEvents - 1)
        to <- (from + 1 to 2 * nrOfEvents)
      } yield EventStoreRange(EventStoreVersion(from), Some(EventStoreVersion(to)))

      val allPossible = (1 to nrOfEvents).map(nr => EventReference[TheTestDomainModel](TestEventThread(nr, 1, 0), EventStoreVersion(2 * nr - 1)))
      possibleRanges foreach { range =>
        val moreThatNeeded = 100
        val aggregateView = eventStore.loadEventsForAggregate(TestAggregateThread(1), range, moreThatNeeded)
        aggregateView.allRangeExtracted shouldBe true
        aggregateView.events shouldBe allPossible
          .filter(e => e.version.storeVersion >= range.from.storeVersion && e.version.storeVersion < range.to.get.storeVersion) withClue s". Extraction for range ${range} is incorrect"

        if (aggregateView.events.nonEmpty) {
          val smaller = aggregateView.events.size - 1
          val aggregateViewLimited = eventStore.loadEventsForAggregate(TestAggregateThread(1), range, smaller)
          aggregateViewLimited.allRangeExtracted shouldBe false
          aggregateViewLimited.events should have size smaller
        }
      }
    }

    it should "trigger listeners when events persisted" taggedAs testsTag in {
      //given
      val eventStore = eventStoreCreator()
      //and a listener
      var counter = 0
      eventStore.registerUpdateListener(() => counter = counter + 1)
      //when events are added
      val addFunc: (Int) => Unit = addOneEvent(eventStore)
      addFunc(1)
      addFunc(2)
      addFunc(3)

      //then
      counter should be(3)
    }
    it should "trigger listeners for aggregated when events persisted" taggedAs testsTag in {
      //given
      val eventStore = eventStoreCreator()
      //and a listener
      var counter = 0
      eventStore.registerAggregateListener(TestAggregateThread(1), () => counter = counter + 1)
      //when events are added
      val addFunc: (Int) => Unit = addOneEvent(eventStore)
      addFunc(1)
      addFunc(2)
      addFunc(3)

      //then
      counter should be(1)
    }

    it should "range [2,4] and [3,inf) is correctly extracted" taggedAs testsTag in {
      //given
      val eventStore = eventStoreCreator()
      //and a number of events to generate
      val nrOfEvents = 10
      val rangeClose: EventStoreRange = EventStoreRange(EventStoreVersion(2L), Some(EventStoreVersion(5L)))
      val rangeOpen: EventStoreRange = EventStoreRange(EventStoreVersion(3L), None)
      //and a to load ranges is run before the events creation

      //when events are added
      val addFunc: (Int) => Unit = addOneEvent(eventStore)
      (1 to nrOfEvents).foreach(addFunc)
      //then
      val load1To4EventsClose = eventStore.loadEvents(rangeClose)
      val load1To4EventsOpen = eventStore.loadEvents(rangeOpen)
      load1To4EventsClose.toList shouldBe List(TestEventThread(0, 2, 0), TestEventThread(0, 3, 0), TestEventThread(0, 4, 0))
      load1To4EventsOpen.toList shouldBe (3 to 10).map(TestEventThread(0, _, 0)).toList
      //and for all ranges:
      for {
        end <- (3 to 10)
      } eventStore.loadEvents(EventStoreRange(EventStoreVersion(2L), Some(EventStoreVersion(end)))) shouldBe (2 to end - 1).map(TestEventThread(0, _, 0))
    }

    it should "extract events for a root aggregate" in {
      //given
      val eventStore = eventStoreCreator()
      //and a number of aggregates
      val nrOfAggregates = 10

      //when events are added on thread root aggregates
      val addFunc: (Int, Int) => Unit = addOneEventOnThreadRoot(eventStore)
      for {
        thread <- 0 to 4
        aggregateNr <- 1 to nrOfAggregates
      } addFunc(thread, aggregateNr)
        //then each aggregate have it own events

        def eventsOnAggregate(aggNr: Int): Seq[TheTestEvent] = (0 to 4).map(TestEventThread(_, aggNr, 0))

      for {
        aggregateNr <- 1 to nrOfAggregates
      } eventStore.loadEventsForAggregate(TestAggregateThread(aggregateNr), EventStoreRange(EventStoreVersion.zero), 5).events.map(_.event) should be(eventsOnAggregate(aggregateNr))

    }

      //
      def addOneEvent(eventStore: EventStore[TheTestDomainModel])(aggregateNr: Int): Unit = {
        addOneEventOnThreadRoot(eventStore)(0, aggregateNr)
      }

      def addOneEventOnThreadRoot(eventStore: EventStore[TheTestDomainModel])(threadNr: Int, aggregateNr: Int): Unit = {
        val atomicTransactionScope = eventStore.calculateAtomicTransactionScopeVersion(new TheTestDomainLogic(), TestCommandForThreads(threadNr, aggregateNr, 0)).toOption.get
        val addResult = eventStore.persistEvents(List(TestEventThread(threadNr, aggregateNr, 0)), TestAggregateThread(aggregateNr), atomicTransactionScope)
        addResult shouldBe \/-
      }

    it should "reject a transaction if the root aggregate already exist and it is not provided in the initial command transaction scope" in {
      //given and event store
      val eventStore = eventStoreCreator()
      //and a aggregate
      val aggregateNr = 0
      //and the aggregate do not have version 0, it means that some events have been stored on the aggregate scope
      val atomicTransactionScope = eventStore.calculateAtomicTransactionScopeVersion(new TheTestDomainLogic(), TestCommandForThreads(0, aggregateNr, 0)).toOption.get
      val firstResult = eventStore.persistEvents(List(TestEventThread(0, aggregateNr, 0)), TestAggregateThread(aggregateNr), atomicTransactionScope)
      firstResult shouldBe \/-
      //when a second transaction is created without the aggregate on the initial transaction scope
      val anyOtherAggregate = 1
      val secondAtomicTransactionScope = eventStore.calculateAtomicTransactionScopeVersion(new TheTestDomainLogic(), TestCommandForThreads(0, anyOtherAggregate, 0)).toOption.get
      val secondResult = eventStore.persistEvents(List(TestEventThread(0, aggregateNr, 0)), TestAggregateThread(aggregateNr), secondAtomicTransactionScope)
      secondResult shouldBe -\/
    }

    it should "detect concurrent execution of updates in the same constraint transaction scope and aggregate transaction scope" taggedAs (testsTag, ConcurrencyTest) in {
      val calculationTime: Int = 4000
      runConcurrentEventPersistence(
        thread => 0, // Aggregate scope is 0 for each thread
        thread => 0, // Constraint scope is 0 for each thread
        expectedRollbackResult = true,
        expectedDeadLockResult = isolationLevelIsSerializable,
        calculationTime,
        eventStoreCreator
      )
    }

    it should "detect concurrent execution of updates in the same aggregate transaction scope" taggedAs (testsTag, ConcurrencyTest) in {
      val calculationTime: Int = 4000
      runConcurrentEventPersistence(
        thread => 0, // Aggregate scope is 0 for each thread
        thread => thread, // Constraint scope different for each thread
        expectedRollbackResult = true,
        expectedDeadLockResult = isolationLevelIsSerializable,
        calculationTime,
        eventStoreCreator
      )
    }

    it should "detect concurrent execution of updates in the same constraint transaction scope" taggedAs (testsTag, ConcurrencyTest) in {
      val calculationTime: Int = 4000
      runConcurrentEventPersistence(
        thread => thread, // Aggregate scope different for each thread
        thread => 0, // Constraint scope is 0 for each thread
        expectedRollbackResult = true,
        expectedDeadLockResult = isolationLevelIsSerializable,
        calculationTime,
        eventStoreCreator,
        ignoreLakeOfRollbackResults = isolationLevelIsSerializable //For serializable all the transactions on h2 get deadlock, so do not check the rollback case
      )
    }

    it should "run concurrently in different aggregate and constraint transaction scope" taggedAs (testsTag, ConcurrencyTest) in {
      val calculationTime: Int = 2000
      runConcurrentEventPersistence(
        thread => thread, // Aggregate scope different for each thread
        thread => thread, // Constraint scope different for each thread
        expectedRollbackResult = false,
        expectedDeadLockResult = isolationLevelIsSerializable,
        calculationTime,
        eventStoreCreator
      )
    }
  }

  def runConcurrentEventPersistence(
    aggregateIdForThreadF:  Int => Int,
    constraintIdForThreadF: Int => Int,
    expectedRollbackResult: Boolean, expectedDeadLockResult: Boolean, calculationTime: Int,
    eventStoreCreator:           () => EventStore[TheTestDomainModel],
    ignoreLakeOfRollbackResults: Boolean                              = false
  ): Unit = {
    //given
    val eventStore = eventStoreCreator()
    //and
    val nrOfThreads = 4
    val barriers = new CountDownLatch(nrOfThreads)
    val rollbackError = new AtomicBoolean(false)
    val deadlockError = new AtomicBoolean(false)
    val endThreadsFlag = new AtomicBoolean(false)
    val domainLogic = new TheTestDomainLogic()
    val errorOnExecution = new AtomicReference[Option[Throwable]](None)

    //when nrOfThreads thread are executed in parallel to store on a transaction context Map(TestAggregate(1) -> aggregateIdForThreadF(threadNr))
    val service: ExecutorService = Executors.newFixedThreadPool(nrOfThreads)
    (1 to nrOfThreads) foreach { thread =>
      service.execute(new Runnable {
        override def run(): Unit = {
          var counter = 0
          var testDone = false
          try {
            while (!Thread.interrupted() && !endThreadsFlag.get() && !testDone) {
              val aggregateIdForThread = aggregateIdForThreadF(thread)
              val constraintIdForThread = constraintIdForThreadF(thread)
              counter = counter + 1
              val scopeCalculation: CommandToAtomicState[TheTestDomainModel] = eventStore.calculateAtomicTransactionScopeVersion(
                domainLogic,
                TestCommandForThreads(thread, aggregateIdForThread, constraintIdForThread)
              )
              scopeCalculation match {
                case -\/(scopeError) =>
                  scopeError match {
                    case EventSourceCommandScopeDeadLock(_) => deadlockError.set(true)
                    case _                                  => fail("A transaction scope calculation has failed with something different than a dead lock error")
                  }
                case \/-(scope) =>
                  eventStore.persistEvents(List(TestEventThread(thread, counter, constraintIdForThread)), TestAggregateThread(thread), scope) match {
                    case -\/(error) => error match {
                      case EventSourceCommandScopeDeadLock(_)               => deadlockError.set(true)
                      case EventSourceCommandRollback(_, _)                 => rollbackError.set(true)
                      case EventSourceCommandFailed(_, _, _)                =>
                      case EventSourceCommandTransactionCalculationError(_) =>
                      case EventSourceCommandInternalError(throwable)       => //TODO
                    }
                    case \/-(ok) =>
                  }
              }
              //check test end condition
              if (ignoreLakeOfRollbackResults) {
                //For isolation level serializable
                //If at least one of the errors is expected to occur
                if (expectedDeadLockResult) {
                  // and the expected result match the current status
                  if (expectedDeadLockResult == deadlockError.get()) {
                    //then I can finish this thread test.
                    testDone = true
                  }
                }
              }
              else {
                //If at least one of the errors is expected to occur
                if (expectedRollbackResult || expectedDeadLockResult) {
                  // and the expected result match the current status
                  if (expectedRollbackResult == rollbackError.get() && expectedDeadLockResult == deadlockError.get()) {
                    //then I can finish this thread test.
                    testDone = true
                  }
                }
              }
            }
          }
          catch {
            case e: InterruptedException =>
              if (endThreadsFlag.get()) {
                // it is ok, because the execution service shutdown
              }
              else {
                errorOnExecution.set(Some(e))
              }
            case e: Throwable => errorOnExecution.set(Some(e))
          }
          finally {
            barriers.countDown()
          }
        }
      })
    }
    barriers.await(calculationTime, TimeUnit.MILLISECONDS)
    endThreadsFlag.set(true)
    val timeToCloseThread = 100
    service.shutdownNow()
    barriers.await(timeToCloseThread, TimeUnit.MILLISECONDS)

    //then a error related to concurrency transaction scopes must be found
    // but no exception should be throw
    if (errorOnExecution.get().isDefined) {
      //Throw the internal exception to get the internal error trace
      throw errorOnExecution.get().get
    }
    // For serializable isolation level, it happens that all transactions get deadlock and rollback is not archived. So do not enforce check for rollback case
    if (!ignoreLakeOfRollbackResults) {
      rollbackError.get() shouldBe expectedRollbackResult
    }
    deadlockError.get() shouldBe expectedDeadLockResult
  }
}

// $COVERAGE-ON$