/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.test

import com.eiipii.esddd.eventsourcing.api._
import com.eiipii.esddd.eventsourcing.eventstore.EventSerializationSchema
import com.eiipii.esddd.eventsourcing.logic._
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel
import com.eiipii.esddd.eventsourcing.projections.EventsProjection
import org.scalacheck.Gen
import org.scalacheck.Gen.const

abstract class AbstractDomainCoreTest[D <: DomainCoreModel, P] extends DomainTestSuiteTools with CommandExecutionHistoryShrink[D, P] {
  //API
  def domainInstanceFromLogic[T <: DomainCoreModel](domainLogic: DomainCoreLogic[T]): DomainInstance[T]

  //Execution
  final def commandExecutionEnvironment(domainLogic: DomainCoreLogic[D]): EventCommandExecution[D] = domainInstanceFromLogic(domainLogic).commandExecution

  final def commandExecutionEnvironment(): EventCommandExecution[D] = domainInstanceFromLogic(logic()).commandExecution

  //Login to test
  def logic(): DomainCoreLogic[D]

  def projection(): EventsProjection[D, P]

  def commandGenerator(): DomainCoreCommandGenerator[D, P]

  def validation: DomainCoreModelValidation[D, P]

  def genCommandExecutionHistory(): Gen[CommandExecutionHistory[D, P]] = for {
    domainAfterExecution <- genDomainStateAfterCommandsExecution()
  } yield domainAfterExecution.history

  def genDomainStatusAfterExecution(): Gen[FinalCommandExecutionResult[D]] = for {
    domainAfterExecution <- genDomainStateAfterCommandsExecution()
    if (!domainAfterExecution.history.commandsAndResults.isEmpty)
  } yield domainAfterExecution.history.commandsAndResults.last.result

  private def genDomainStateAfterCommandsExecution(): Gen[DomainStateAfterCommandsExecution[D, P]] = {
      def sizedCommandExecution(logicInst: DomainCoreLogic[D], domainApi: EventCommandExecution[D])(sz: Int): Gen[(GenerationState[D, P], List[SingleCommandExecution[D, P]])] = {
        val l: List[Unit] = List.fill(sz)(())
        val generator: DomainCoreCommandGenerator[D, P] = commandGenerator()
        val projector: EventsProjection[D, P] = projection()
        val initialState: GenerationState[D, P] = GenerationState[D, P](logicInst.atomicProjectionProjection.zero.state, projector.zero, Seq())
        l.foldLeft(const((initialState, Nil: List[SingleCommandExecution[D, P]]))) {
          case (z, ()) =>
            for {
              (previousState, history) <- z
              c <- generator.genCommand(previousState.state, previousState.projection)
            } yield {
              val result: FinalCommandToEventsResult[D] = domainApi.execute(c)
              assert(result.isRight, s", Generated commands must run without errors, the command $c failed in the result $result")
              val confirmation: FinalCommandExecutionResult[D] = result.toOption.get
              val nextProjectionState: P = projector.projectEvents(previousState.projection, confirmation.events)
              (
                GenerationState(confirmation.state, nextProjectionState, confirmation.events),
                history :+ SingleCommandExecution[D, P](c, confirmation, nextProjectionState)
              )
            }
        }
      }

    for {
      logicInst <- Gen.const(logic())
      commandApi <- Gen.const(commandExecutionEnvironment(logicInst))
      history <- Gen.sized(sizedCommandExecution(logicInst, commandApi)).map(_._2)
    } yield DomainStateAfterCommandsExecution[D, P](CommandExecutionHistory[D, P](history))
  }
}

private case class GenerationState[D <: DomainCoreModel, P](state: D#State, projection: P, events: Seq[D#Event])

trait DomainCoreModelValidation[D <: DomainCoreModel, P] {

  def postCommandValidation(command: D#Command, rootAggregate: D#Aggregate, state: D#State, projection: P): Unit

  def validateState(state: D#State, projection: P): Unit

}

trait StandardCommandHistoryTest[D <: DomainCoreModel, P] {
  self: AbstractDomainCoreTest[D, P] =>
  it should "Generate valid execution states and event source history" in {
    forAll(genCommandExecutionHistory()) { history =>
      //Validate all command executions
      history.commandsAndResults.foreach { se =>
        validation.validateState(se.result.state, se.projection) withClue s": On state validation after execution of $se"
        validation.postCommandValidation(se.command, se.result.rootAggregate, se.result.state, se.projection) withClue s": On Command validation after execution of $se"
      }
    }
  }

}

trait SchemaValidationTest[D <: DomainCoreModel] {
  self: AbstractDomainCoreTest[D, _] =>

  def schema: EventSerializationSchema[D]

  it should "Serialize and Deserialize using the provided schema" in {
    forAll(genDomainStatusAfterExecution()) { domainState =>
      //Validate all command executions
      domainState.events.foreach { event =>
        val remapped = schema.mapToEvent(schema.eventToData(event))
        remapped shouldBe (event)
      }
    }
  }
}