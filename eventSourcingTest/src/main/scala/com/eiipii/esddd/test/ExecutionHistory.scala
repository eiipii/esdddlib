/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.test

import com.eiipii.esddd.eventsourcing.logic.FinalCommandExecutionResult
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel
import org.scalacheck.Shrink

class ExecutionHistory {

}

/** A reference to a generated domain api and a successful execution history.
 *  No Shrink is provided, because domain status can not be rollbacked to previous states.
 *
 *  @param history   History of commands and results
 *  @tparam D The domain model
 */
case class DomainStateAfterCommandsExecution[D <: DomainCoreModel, P](history: CommandExecutionHistory[D, P])

/** A list of successful commands and results execution on the domain.
 *  The only valid shrink strategy is to remove the history entry, because commands generation depends on the domain state.
 *
 *  @param commandsAndResults execution history
 *  @tparam D the domain model
 */
case class CommandExecutionHistory[D <: DomainCoreModel, P](commandsAndResults: List[SingleCommandExecution[D, P]])

case class EventStoreDomainState[D <: DomainCoreModel](state: D#State, events: Seq[D#Event])

case class SingleCommandExecution[D <: DomainCoreModel, P](command: D#Command, result: FinalCommandExecutionResult[D], projection: P)

trait CommandExecutionHistoryShrink[D <: DomainCoreModel, P] {
  // Produce the history in order
  implicit val shrinkHistory = Shrink[CommandExecutionHistory[D, P]] {
    shrinkByOne
  }

  private def shrinkByOne(ceh: CommandExecutionHistory[D, P]): Stream[CommandExecutionHistory[D, P]] = {
    if (ceh.commandsAndResults.nonEmpty) {
      List(CommandExecutionHistory[D, P](ceh.commandsAndResults.take(ceh.commandsAndResults.size - 1))).toStream
    }
    else {
      Stream.empty
    }
  }
}