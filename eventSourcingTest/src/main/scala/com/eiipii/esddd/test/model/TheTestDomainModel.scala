/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.test.model

import com.eiipii.esddd.eventsourcing.eventstore.EventSerializationSchema
import com.eiipii.esddd.eventsourcing.logic._
import com.eiipii.esddd.eventsourcing.model._
import com.eiipii.esddd.eventsourcing.projections.EventsProjection
import com.eiipii.esddd.eventsourcing.{AggregateReference, ConstraintReference, EventData}

import scalaz._

final class TheTestDomainModel extends DomainCoreModel {
  type Command = TheTestCommand
  type Event = TheTestEvent
  type Aggregate = TheTestAggregate
  type ConstraintScope = TheTestConstraintScope
  type State = TheTestState
}

//Model entities

//Aggregate
sealed trait TheTestAggregate

case class TestAggregateOne() extends TheTestAggregate

case class TestAggregateTwo() extends TheTestAggregate

case class TestAggregateThread(nr: Long) extends TheTestAggregate

//Constraints
sealed trait TheTestConstraintScope

case class TestConstraintOne() extends TheTestConstraintScope

case class TestConstraintTwo(constraintNr: Long) extends TheTestConstraintScope

//UserRegistrationModel commands

sealed trait TheTestCommand

case class TestCommandOne() extends TheTestCommand

case class TestCommandTwo(createTwo: Boolean) extends TheTestCommand

case class TestCommandForThreads(threadNr: Int, targetAggregate: Int, targetConstraint: Int) extends TheTestCommand

//UserRegistrationModel events

sealed trait TheTestEvent

case class TestEventOne() extends TheTestEvent

case class TestEventTwo() extends TheTestEvent

case class TestEventThree() extends TheTestEvent

case class TestEventThread(threadNr: Int, aggregate: Int, constraint: Int) extends TheTestEvent

/** Projection that should be changes atomically with relation to the handled commands.
 */
trait TheTestState {

  def history(): String

  def countOne(): Int

  def countTwo(): Int

  def countThree(): Int

  def lastAdded(): Int

  def events: List[TheTestEvent]
}

class TheTestDomainLogic extends DomainCoreLogic[TheTestDomainModel] {

  override def calculateAggregates(command: TheTestCommand, state: TheTestState): DomainCommandToAggregateScope[TheTestDomainModel] =
    command match {
      case TestCommandOne() => \/-(Set(TestAggregateOne()))
      case TestCommandTwo(createTwo) => \/-(Set(TestAggregateTwo()))
      case TestCommandForThreads(threadNr, targetAggregate, targetConstraint) => \/-(Set(TestAggregateThread(targetAggregate)))
    }

  override def calculateConstraints(command: TheTestCommand, state: TheTestState): DomainCommandToConstraints[TheTestDomainModel] =
    command match {
      case TestCommandOne() => \/-(Set(TestConstraintOne()))
      case TestCommandTwo(createTwo) => \/-(Set(TestConstraintOne()))
      case TestCommandForThreads(threadNr, targetAggregate, targetConstraint) => \/-(Set(TestConstraintTwo(targetConstraint)))
    }

  override def calculateCommandToEvents(command: TheTestCommand)(implicit state: TheTestState): DomainCommandToEventsResult[TheTestDomainModel] =
    command match {
      case TestCommandForThreads(threadNr, targetAggregate, targetConstraint) =>
        if (threadNr < 0) {
          -\/(DomainFailure(DomainErrorCode(threadNr)))
        }
        else {
          \/-(CommandExecutionResult(List(TestEventThread(threadNr, targetAggregate, targetConstraint)), TestAggregateThread(targetAggregate)))
        }
      case TestCommandOne() =>
        \/-(CommandExecutionResult(List(TestEventOne()), TestAggregateOne()))
      case TestCommandTwo(createTwo) => if (createTwo) {
        \/-(CommandExecutionResult(List(TestEventTwo()), TestAggregateTwo()))
      }
      else {
        \/-(CommandExecutionResult(List(TestEventThree()), TestAggregateTwo()))
      }
    }

  override def atomicProjectionProjection: EventStoreAtomicProjectionPoint[TheTestDomainModel] =
    new StateAtomicProjectionPoint(
      new TheEventStateProjection()
    )

  override def equalityRelations: DomainEqualityRelations[TheTestDomainModel] =
    new DomainEqualityRelations[TheTestDomainModel] {
      override def buildAggregateReference(aggregate: TheTestAggregate): AggregateReference = aggregate match {
        case TestAggregateOne()      => AggregateReference(1, 0L)
        case TestAggregateTwo()      => AggregateReference(2, 0L)
        case TestAggregateThread(nr) => AggregateReference(3, nr)
      }

      override def buildConstraintReference(constraintScope: TheTestConstraintScope): Option[ConstraintReference] = constraintScope match {
        case TestConstraintOne()             => Some(ConstraintReference(1, 0L))
        case TestConstraintTwo(constraintNr) => Some(ConstraintReference(2, constraintNr))
      }
    }
}

class TheTestEventSerializationSchema extends EventSerializationSchema[TheTestDomainModel] {

  import org.json4s._
  import org.json4s.native.Serialization._

  val hints = ShortTypeHints(List(
    classOf[TestEventOne],
    classOf[TestEventTwo],
    classOf[TestEventThree],
    classOf[TestEventThread]
  ))
  implicit val formats = native.Serialization.formats(hints)

  override def mapToEvent(data: EventData): TheTestEvent = read[TheTestEvent](new String(data.eventBytes))

  override def eventToData(event: TheTestEvent): EventData = EventData(write(event).getBytes)

}

class TheEventStateProjection extends EventGroupProjection[TheTestDomainModel, TestStateInMemory]
    with EventsProjection[TheTestDomainModel, TestStateInMemory] {
  override lazy val zero: TestStateInMemory = TestStateInMemory()

  override def projectSingleEvent(state: TestStateInMemory, event: TheTestEvent): TestStateInMemory = event match {
    case TestEventOne()                                  => state.copy(one = state.one + 1, lastAdded = 1, history = s"1,${state.history}")
    case TestEventTwo()                                  => state.copy(two = state.two + 1, lastAdded = 2, history = s"2,${state.history}")
    case TestEventThree()                                => state.copy(three = state.three + 1, lastAdded = 3, history = s"3,${state.history}")
    case e @ TestEventThread(threadNr, data, constraint) => state.copy(events = state.events :+ e, history = s"t$threadNr,${state.history}")
  }

}

case class TestStateInMemory(
    one:       Int                = 0,
    two:       Int                = 0,
    three:     Int                = 0,
    lastAdded: Int                = 0,
    history:   String             = "",
    events:    List[TheTestEvent] = List()
) extends TheTestState {
  override def countOne(): Int = one

  override def countTwo(): Int = two

  override def countThree(): Int = three

}
