/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.example.todo.model

import com.eiipii.esddd.eventsourcing.model._

trait TodoCoreModel extends DomainCoreModel {
  type Command = ToDoCommand
  type Event = ToDoEvent
  type Aggregate = ToDoAggregate
  type ConstraintScope = ToDoConstraintScope
  type State = ToDoState
}

object TodoCoreModel {
  val todoIdNotOnDatabaseError = -1
  val todoIdNotOnDatabase = DomainErrorCode(todoIdNotOnDatabaseError)
  val listIdAndTodoIdDoNotMatchError = -2
  val listIdAndTodoIdDoNotMatch = DomainErrorCode(listIdAndTodoIdDoNotMatchError)
  val invalidOwnerReferenceError = -3
  val invalidOwnerReference = DomainErrorCode(invalidOwnerReferenceError)
  val listIDDoNotExistError = -3
  val listIDDoNotExist = DomainErrorCode(listIDDoNotExistError)
}

case class TodoID(listID: ListID, todoNr: Int)

sealed trait ToDoCommand

case class CreateToDoList(ownerUniqueReference: String) extends ToDoCommand
case class DeleteToDoList(listID: ListID) extends ToDoCommand

case class AddToDo(listID: ListID, todo: String) extends ToDoCommand
case class MarkAsDone(listID: ListID, todoID: TodoID) extends ToDoCommand
case class MarkAsUndone(listID: ListID, todoID: TodoID) extends ToDoCommand
case class DeleteToDo(listID: ListID, todoID: TodoID) extends ToDoCommand
case class UpdateToDo(listID: ListID, todo: String, todoID: TodoID) extends ToDoCommand

sealed trait ToDoEvent

case class ToDoListCreated(owner: String, listID: ListID) extends ToDoEvent
case class ToDoListDeleted(listID: ListID) extends ToDoEvent

case class ToDoAdded(listID: ListID, todo: String, todoID: TodoID) extends ToDoEvent
case class ToDoDone(listID: ListID, todoID: TodoID) extends ToDoEvent
case class ToDoUndone(listID: ListID, todoID: TodoID) extends ToDoEvent
case class ToDoDeleted(listID: ListID, todoID: TodoID) extends ToDoEvent
case class ToDoUpdated(listID: ListID, todo: String, todoID: TodoID) extends ToDoEvent

sealed trait ToDoAggregate

case class ToDoList(listID: ListID) extends ToDoAggregate

sealed trait ToDoConstraintScope

case class ListIDUnique(listID: ListID) extends ToDoConstraintScope

trait ToDoState {
  def getNextListID: ListID
  def listExist(listID: ListID): Boolean
  def getListToDos(listID: ListID): Seq[TodoID]
  def getNextNrForList(listID: ListID): Int
}

case class ToDoStatistics(totalNrOfTodos: Int)

case class ListID(val id: Long) extends AnyVal