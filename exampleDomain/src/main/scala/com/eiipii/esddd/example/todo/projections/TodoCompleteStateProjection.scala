/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.example.todo.projections

import com.eiipii.esddd.eventsourcing.projections.EventsProjection
import com.eiipii.esddd.example.todo.model._

class TodoCompleteStateProjection extends EventsProjection[TodoCoreModel, TodoCompleteState] {
  override def zero(): TodoCompleteState = TodoCompleteState()

  override def projectSingleEvent(state: TodoCompleteState, event: ToDoEvent): TodoCompleteState = event match {
    case ToDoListCreated(owner, listID) => TodoCompleteState(
      state.nextListID + 1,
      state.todos.updated(
        listID, Seq()
      ),
      state.todoDone,
      state.currentMaxUserToDoIDs,
      state.todoText,
      state.owners.updated(listID, owner)
    )
    case ToDoListDeleted(listID) => TodoCompleteState(
      state.nextListID,
      state.todos - listID,
      state.todoDone,
      state.currentMaxUserToDoIDs,
      state.todoText,
      state.owners - listID
    )
    case ToDoAdded(listID, todo, todoID) => TodoCompleteState(
      state.nextListID,
      state.todos.updated(
        listID,
        state.todos(listID) ++ Seq(todoID)
      ),
      state.todoDone,
      state.currentMaxUserToDoIDs + (listID -> (state.getNextNrForList(listID) + 1)),
      state.todoText.updated(
        todoID,
        todo
      ),
      state.owners
    )
    case ToDoDone(listID, todoID) => TodoCompleteState(
      state.nextListID,
      state.todos,
      state.todoDone + (todoID -> true),
      state.currentMaxUserToDoIDs,
      state.todoText,
      state.owners
    )
    case ToDoUndone(listID, todoID) => TodoCompleteState(
      state.nextListID,
      state.todos,
      state.todoDone + (todoID -> false),
      state.currentMaxUserToDoIDs,
      state.todoText,
      state.owners
    )
    case ToDoDeleted(listID, todoID) => TodoCompleteState(
      state.nextListID,
      state.todos.updated(
        listID,
        state.todos(listID) diff Seq(todoID)
      ),
      state.todoDone - todoID,
      state.currentMaxUserToDoIDs,
      state.todoText - todoID,
      state.owners
    )
    case ToDoUpdated(listID, todo, todoID) => TodoCompleteState(
      state.nextListID,
      state.todos,
      state.todoDone,
      state.currentMaxUserToDoIDs,
      state.todoText + (todoID -> todo),
      state.owners
    )
  }
}

case class TodoCompleteState(
    nextListID:            Long                     = 0,
    todos:                 Map[ListID, Seq[TodoID]] = Map(),
    todoDone:              Map[TodoID, Boolean]     = Map(),
    currentMaxUserToDoIDs: Map[ListID, Int]         = Map(),
    todoText:              Map[TodoID, String]      = Map(),
    owners:                Map[ListID, String]      = Map()
) extends ToDoState {

  override def getListToDos(listID: ListID): Seq[TodoID] = todos(listID)

  override def getNextNrForList(listID: ListID): Int = currentMaxUserToDoIDs.getOrElse(listID, 0)

  def getToDoStatistics: Map[ListID, ToDoStatistics] = todos.mapValues(v => ToDoStatistics(v.size))

  def isTodoDone(todoID: TodoID): Boolean = todoDone.getOrElse(todoID, false)

  def getToDo(todoID: TodoID): Option[String] = todoText.get(todoID)

  override def getNextListID(): ListID = ListID(nextListID)

  override def listExist(listID: ListID): Boolean = todos.contains(listID)
}