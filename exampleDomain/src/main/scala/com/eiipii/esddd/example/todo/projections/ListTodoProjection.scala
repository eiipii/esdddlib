/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.example.todo.projections

import com.eiipii.esddd.eventsourcing.logic._
import com.eiipii.esddd.example.todo.model._

object ListTodoProjection {

  import monocle._
  import monocle.function.Index.index
  import monocle.macros.GenLens

  val todosMap: Lens[ListTodoData, Vector[TodoData]] = GenLens[ListTodoData](_.todos)

  def optTodo(todoID: TodoID): Optional[ListTodoData, TodoData] = todosMap composeOptional index(todoID.todoNr)

  val _id: Lens[TodoData, TodoID] = GenLens[TodoData](_.todoID)
  val _title: Lens[TodoData, String] = GenLens[TodoData](_.title)
  val _done: Lens[TodoData, Boolean] = GenLens[TodoData](_.done)
  val _deleted: Lens[TodoData, Boolean] = GenLens[TodoData](_.deleted)
}

class ListTodoProjection(listID: ListID) extends EventGroupProjection[TodoCoreModel, ListTodoData] {

  import ListTodoProjection._

  override def zero(): ListTodoData = ListTodoData(listID, Vector())

  override def projectSingleEvent(state: ListTodoData, event: ToDoEvent): ListTodoData = event match {
    case ToDoListCreated(owner, listID)     => state
    case ToDoListDeleted(listID)            => state
    case ToDoAdded(listID, title, todoID)   => state.copy(todos = state.todos :+ TodoData(todoID, title))
    case ToDoDone(listID, todoID)           => (optTodo(todoID) composeLens _done).modify(_ => true)(state)
    case ToDoUndone(listID, todoID)         => (optTodo(todoID) composeLens _done).modify(_ => false)(state)
    case ToDoDeleted(listID, todoID)        => (optTodo(todoID) composeLens _deleted).modify(_ => true)(state)
    case ToDoUpdated(listID, title, todoID) => (optTodo(todoID) composeLens _title).modify(_ => title)(state)
  }
}

case class ListTodoData(listID: ListID, todos: Vector[TodoData])

case class TodoData(todoID: TodoID, title: String, done: Boolean = false, deleted: Boolean = false)