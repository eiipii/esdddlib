/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.example.todo.logic

import com.eiipii.esddd.eventsourcing.logic._
import com.eiipii.esddd.eventsourcing.model._
import com.eiipii.esddd.eventsourcing.{AggregateReference, ConstraintReference}
import com.eiipii.esddd.example.todo.model._

import scalaz.{-\/, \/, \/-}

class TodoCoreLogic extends DomainCoreLogic[TodoCoreModel] {

  override def equalityRelations: DomainEqualityRelations[TodoCoreModel] = new DomainEqualityRelations[TodoCoreModel] {
    override def buildAggregateReference(aggregate: ToDoAggregate): AggregateReference =
      aggregate match {
        case ToDoList(listID) => AggregateReference(0, listID.id)
      }

    override def buildConstraintReference(constraintScope: ToDoConstraintScope): Option[ConstraintReference] = constraintScope match {
      case ListIDUnique(listID) => Some(ConstraintReference(0, listID.id))
    }
  }

  override def calculateConstraints(command: ToDoCommand, state: ToDoState): DomainCommandToConstraints[TodoCoreModel] = command match {
    case CreateToDoList(ownerUniqueReference) => \/-(Set(ListIDUnique(state.getNextListID)))
    case DeleteToDoList(listID)               => \/-(Set())
    case AddToDo(listID, todo)                => \/-(Set())
    case MarkAsDone(listID, todoID)           => \/-(Set())
    case MarkAsUndone(listID, todoID)         => \/-(Set())
    case DeleteToDo(listID, todoID)           => \/-(Set())
    case UpdateToDo(listID, todo, todoID)     => \/-(Set())
  }

  override def calculateAggregates(command: ToDoCommand, state: ToDoState): DomainCommandToAggregateScope[TodoCoreModel] = command match {
    case CreateToDoList(ownerUniqueReference) => \/-(Set())
    case DeleteToDoList(listID)               => \/-(Set(ToDoList(listID)))
    case AddToDo(listID, todo)                => \/-(Set(ToDoList(listID)))
    case MarkAsDone(listID, todoID)           => \/-(Set(ToDoList(listID)))
    case MarkAsUndone(listID, todoID)         => \/-(Set(ToDoList(listID)))
    case DeleteToDo(listID, todoID)           => \/-(Set(ToDoList(listID)))
    case UpdateToDo(listID, todo, todoID)     => \/-(Set(ToDoList(listID)))
  }

  override def calculateCommandToEvents(command: ToDoCommand)(implicit state: ToDoState): DomainCommandToEventsResult[TodoCoreModel] = command match {
    case CreateToDoList(ownerUniqueReference) => for {
      validOwner <- validateOwnerRef(ownerUniqueReference)
    } yield {
      val newListID = state.getNextListID
      CommandExecutionResult(List(ToDoListCreated(validOwner, newListID)), ToDoList(newListID))
    }
    case DeleteToDoList(listID) => for {
      validListID <- listExist(listID)
    } yield CommandExecutionResult(List(ToDoListDeleted(validListID)), ToDoList(validListID))
    case AddToDo(listID, todo) => for {
      validListID <- listExist(listID)
    } yield CommandExecutionResult(List(ToDoAdded(listID, todo, TodoID(listID, state.getNextNrForList(validListID)))), ToDoList(validListID))
    case MarkAsDone(listID, todoID) => for {
      validListID <- listExist(listID)
      validTodoID <- validate(listID, todoID)
    } yield CommandExecutionResult(List(ToDoDone(listID, validTodoID)), ToDoList(validListID))
    case MarkAsUndone(listID, todoID) => for {
      validListID <- listExist(listID)
      validTodoID <- validate(listID, todoID)
    } yield CommandExecutionResult(List(ToDoUndone(listID, validTodoID)), ToDoList(validListID))
    case DeleteToDo(listID, todoID) => for {
      validListID <- listExist(listID)
      validTodoID <- validate(listID, todoID)
    } yield CommandExecutionResult(List(ToDoDeleted(listID, validTodoID)), ToDoList(validListID))
    case UpdateToDo(listID, todo, todoID) => for {
      validListID <- listExist(listID)
      validTodoID <- validate(listID, todoID)
    } yield CommandExecutionResult(List(ToDoUpdated(listID, todo, validTodoID)), ToDoList(validListID))
  }

  private def validateOwnerRef(ownerUniqueReference: String)(implicit state: ToDoState): DomainFailure \/ String = {
    if (ownerUniqueReference.startsWith("email:")) {
      \/-(ownerUniqueReference)
    }
    else {
      -\/(DomainFailure(TodoCoreModel.invalidOwnerReference))
    }
  }

  private def listExist(listID: ListID)(implicit state: ToDoState): DomainFailure \/ ListID = {
    if (state.listExist(listID)) {
      \/-(listID)
    }
    else {
      -\/(DomainFailure(TodoCoreModel.listIDDoNotExist))
    }
  }

  private def validate(listID: ListID, todoID: TodoID)(implicit state: ToDoState): DomainFailure \/ TodoID = {
    if (todoID.listID.id != listID.id) {
      -\/(DomainFailure(TodoCoreModel.listIdAndTodoIdDoNotMatch))
    }
    else {
      \/-(todoID)
    }
  }

  override def atomicProjectionProjection: EventStoreAtomicProjectionPoint[TodoCoreModel] =
    new StateAtomicProjectionPoint(
      new TodoGroup()
    )
}

class TodoGroup extends EventGroupProjection[TodoCoreModel, TodoStateImpl] {
  override def zero: TodoStateImpl = TodoStateImpl()

  override def projectSingleEvent(state: TodoStateImpl, event: ToDoEvent): TodoStateImpl = event match {
    case ToDoListCreated(owner, listID) => TodoStateImpl(
      state.nextListID + 1,
      state.todos.updated(listID, Seq()),
      state.currentMaxUserToDoIDs
    )
    case ToDoListDeleted(listID) => TodoStateImpl(
      state.nextListID,
      state.todos - listID,
      state.currentMaxUserToDoIDs
    )
    case ToDoAdded(listID, todo, todoID) => TodoStateImpl(
      state.nextListID,
      state.todos.updated(
        listID,
        state.todos(listID) ++ Seq(todoID)
      ),
      state.currentMaxUserToDoIDs + (listID -> (state.getNextNrForList(listID) + 1))
    )
    case ToDoDone(listID, todoID) => TodoStateImpl(
      state.nextListID,
      state.todos,
      state.currentMaxUserToDoIDs
    )
    case ToDoUndone(listID, todoID) => TodoStateImpl(
      state.nextListID,
      state.todos,
      state.currentMaxUserToDoIDs
    )
    case ToDoDeleted(listID, todoID) => TodoStateImpl(
      state.nextListID,
      state.todos.updated(
        listID,
        state.todos(listID) diff Seq(todoID)
      ),
      state.currentMaxUserToDoIDs
    )
    case ToDoUpdated(listID, todo, todoID) => TodoStateImpl(
      state.nextListID,
      state.todos,
      state.currentMaxUserToDoIDs
    )
  }
}

case class TodoStateImpl(
    nextListID:            Long                     = 0,
    todos:                 Map[ListID, Seq[TodoID]] = Map(),
    currentMaxUserToDoIDs: Map[ListID, Int]         = Map()
) extends ToDoState {

  override def getListToDos(listID: ListID): Seq[TodoID] = todos.getOrElse(listID, Seq())

  override def getNextNrForList(listID: ListID): Int = currentMaxUserToDoIDs.getOrElse(listID, 0)

  override def getNextListID(): ListID = ListID(nextListID)

  override def listExist(listID: ListID): Boolean = todos.contains(listID)
}