/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.example.todo.logic

import com.eiipii.esddd.eventsourcing.DefaultInMemoryEventCommandExecutionForDomainLogic
import com.eiipii.esddd.eventsourcing.api.{CommandDomainError, EventCommandExecution}
import com.eiipii.esddd.eventsourcing.logic.DomainCoreLogic
import com.eiipii.esddd.eventsourcing.projections.EventsProjection
import com.eiipii.esddd.example.todo.model._
import com.eiipii.esddd.example.todo.projections.{TodoCompleteState, TodoCompleteStateProjection}
import com.eiipii.esddd.test._

class TodoCoreLogicTest extends AbstractDomainCoreTest[TodoCoreModel, TodoCompleteState]
    with DefaultInMemoryEventCommandExecutionForDomainLogic
    with StandardCommandHistoryTest[TodoCoreModel, TodoCompleteState] {

  it should "validate owner format" in {
    val api: EventCommandExecution[TodoCoreModel] = commandExecutionEnvironment()
    val commandResult = api.execute(CreateToDoList("invalid"))
    commandResult shouldBe -\/
  }

  it should "validate for matching listID and todoID" in {
    val api: EventCommandExecution[TodoCoreModel] = commandExecutionEnvironment()
    val createdList = api.execute(CreateToDoList("email:anyValid"))
    createdList shouldBe \/-

    val listIDCreated = createdList.toOption.get.rootAggregate match {
      case ToDoList(listID) => listID
    }
    val addResult = api.execute(AddToDo(listIDCreated, "anyTodoText"))
    addResult shouldBe \/-
    addResult.toOption.get.state.getListToDos(listIDCreated) should not be empty
    addResult.toOption.get.state.getListToDos(listIDCreated) should have size 1
    val todoAdded = addResult.toOption.get.state.getListToDos(listIDCreated).head
    val otherListIDNotMatching = ListID(listIDCreated.id + 1)

    val tryToAddWithInvalidTodoID = api.execute(MarkAsDone(listIDCreated, todoAdded.copy(listID = otherListIDNotMatching)))
    tryToAddWithInvalidTodoID shouldBe (scalaz.-\/(CommandDomainError(TodoCoreModel.listIdAndTodoIdDoNotMatch)))
  }

  it should "validate that todo exist on database" in {
    val api: EventCommandExecution[TodoCoreModel] = commandExecutionEnvironment()
    val commandResult = api.execute(MarkAsDone(ListID(1), TodoID(ListID(1), 0)))
    commandResult shouldBe -\/
  }

  //Execution

  override def projection(): EventsProjection[TodoCoreModel, TodoCompleteState] = new TodoCompleteStateProjection()

  override def logic(): DomainCoreLogic[TodoCoreModel] = new TodoCoreLogic()

  override def commandGenerator(): DomainCoreCommandGenerator[TodoCoreModel, TodoCompleteState] = new TodoModelGenerator()

  override def validation: DomainCoreModelValidation[TodoCoreModel, TodoCompleteState] = new DomainCoreModelValidation[TodoCoreModel, TodoCompleteState] {

    override def validateState(state: ToDoState, projection: TodoCompleteState): Unit = {
      state.getListToDos(ListID(-1)) should be(empty) withClue "For not existing user, a empty todo list should be provided"
      state.getNextListID shouldBe projection.getNextListID()
      projection.getToDoStatistics.foreach {
        case (user, stats) =>
          state.getListToDos(user).size should be(stats.totalNrOfTodos)
      }
    }

    override def postCommandValidation(command: ToDoCommand, rootAggregate: ToDoAggregate, state: ToDoState, projection: TodoCompleteState): Unit = {
      command match {
        case CreateToDoList(ownerUniqueReference) =>
          val created: ListID = rootAggregate match {
            case ToDoList(listID) => listID
          }
          state.getNextListID shouldBe projection.getNextListID()
          state.getListToDos(created) shouldBe empty
          state.listExist(created) shouldBe true
          projection.listExist(created) shouldBe true
          projection.owners.get(created) shouldBe Some(ownerUniqueReference)
        case DeleteToDoList(listID) =>
          state.listExist(listID) shouldBe false
        case AddToDo(listID, todo) =>
          state.getListToDos(listID) should not be empty
          projection.getListToDos(listID) should be(state.getListToDos(listID))
          rootAggregate should be(ToDoList(listID))
          val expectedTodoId = projection.getListToDos(listID).last
          projection.getToDo(expectedTodoId) shouldBe Some(todo)
        case MarkAsDone(listID, todoID) =>
          projection.isTodoDone(todoID) should be(true) withClue "After MarkAsDone, looking by todoID do not show done."
          projection.getToDo(todoID) should not be empty
        case MarkAsUndone(listID, todoID) =>
          projection.isTodoDone(todoID) should be(false) withClue "After MarkAsUndone, looking by todoID do not show undone."
          projection.getToDo(todoID) should not be empty
        case DeleteToDo(listID, todoID) =>
          state.getListToDos(listID) should not contain todoID withClue ", but after delete of the todo, it should not be listed on the users todo's"
          projection.getToDo(todoID) shouldBe empty
        case UpdateToDo(listID, todo, todoID) =>
          projection.getToDo(todoID) should be(Some(todo))
      }
    }
  }

}