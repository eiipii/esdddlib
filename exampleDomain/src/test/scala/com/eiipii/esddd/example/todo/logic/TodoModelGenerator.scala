/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.example.todo.logic

import com.eiipii.esddd.example.todo.model._
import com.eiipii.esddd.example.todo.projections.TodoCompleteState
import com.eiipii.esddd.test._
import org.scalacheck.Gen._
import org.scalacheck._

class TodoModelGenerator extends DomainCoreCommandGenerator[TodoCoreModel, TodoCompleteState] {

  override def genCommand(state: ToDoState, projection: TodoCompleteState): Gen[ToDoCommand] =
    if (availableLists(state, projection).isEmpty) {
      genCreateToDoList(state, projection)
    }
    else if (usersWithNotEmptyLists(state, projection).isEmpty) {
      genAddToDo(state, projection)
    }
    else {
      frequency(
        (2, genCreateToDoList(state, projection)),
        (1, genDeleteToDoList(state, projection)),
        (2, genMarkAsDone(state, projection)),
        (2, genMarkAsUndone(state, projection)),
        (2, genDeleteToDo(state, projection)),
        (2, genUpdateToDo(state, projection)),
        (5, genAddToDo(state, projection))
      )
    }

  def availableLists(state: ToDoState, projection: TodoCompleteState): Seq[ListID] = projection.todos.keys.toSeq

  def usersWithNotEmptyLists(state: ToDoState, projection: TodoCompleteState): Map[ListID, ToDoStatistics] =
    projection.getToDoStatistics.filter {
      case (listID, statistics) => statistics.totalNrOfTodos > 0
    }

  def genMarkAsDone(state: ToDoState, projection: TodoCompleteState): Gen[MarkAsDone] = for {
    notEmpty <- const(usersWithNotEmptyLists(state, projection))
    listID <- oneOf(notEmpty.keys.toSeq)
    allTodos <- const(state.getListToDos(listID))
    todoID <- oneOf(allTodos)
  } yield MarkAsDone(listID, todoID)

  def genMarkAsUndone(state: ToDoState, projection: TodoCompleteState): Gen[MarkAsUndone] = for {
    notEmpty <- const(usersWithNotEmptyLists(state, projection))
    listID <- oneOf(notEmpty.keys.toSeq)
    allTodos <- const(state.getListToDos(listID))
    todoID <- oneOf(allTodos)
  } yield MarkAsUndone(listID, todoID)

  def genDeleteToDo(state: ToDoState, projection: TodoCompleteState): Gen[DeleteToDo] = for {
    notEmpty <- const(usersWithNotEmptyLists(state, projection))
    listID <- oneOf(notEmpty.keys.toSeq)
    allTodos <- const(state.getListToDos(listID))
    todoID <- oneOf(allTodos)
  } yield DeleteToDo(listID, todoID)

  def genUpdateToDo(state: ToDoState, projection: TodoCompleteState): Gen[UpdateToDo] = for {
    notEmpty <- const(usersWithNotEmptyLists(state, projection))
    listID <- oneOf(notEmpty.keys.toSeq)
    allTodos <- const(state.getListToDos(listID))
    todoID <- oneOf(allTodos)
    todoString <- Gen.alphaStr
  } yield UpdateToDo(listID, todoString, todoID)

  def genAddToDo(state: ToDoState, projection: TodoCompleteState): Gen[AddToDo] = for {
    listID <- Gen.oneOf(projection.todos.keys.toSeq)
    todoString <- Gen.alphaStr
  } yield AddToDo(listID, todoString)

  def genCreateToDoList(state: ToDoState, projection: TodoCompleteState): Gen[CreateToDoList] = for {
    owner <- genOwner
  } yield CreateToDoList(owner)

  def genDeleteToDoList(state: ToDoState, projection: TodoCompleteState): Gen[DeleteToDoList] = for {
    toDelete <- Gen.oneOf(projection.todos.keys.toSeq)
  } yield DeleteToDoList(toDelete)

  lazy val genOwner = for {
    nr <- Gen.choose(minOwnerNr, maxOwnerNr)
  } yield s"email:test${nr}@eiipii.com"

  val minOwnerNr = 0L
  val maxOwnerNr = 10L

}