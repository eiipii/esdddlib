/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package com.eiipii.esddd.example.todo.projections

import akka._
import akka.actor.ActorSystem
import akka.event.Logging
import akka.stream.{ActorMaterializer, Attributes}
import akka.stream.scaladsl._
import akka.stream.testkit.TestSubscriber.Probe
import akka.stream.testkit.scaladsl.TestSink
import com.eiipii.esddd.eventsourcing.ESDDD
import com.eiipii.esddd.eventsourcing.eventstore.inmemory.InMemoryEventStoreBackend
import com.eiipii.esddd.eventsourcing.stream.akka.{EventStoreFlow, EventStoreSource}
import com.eiipii.esddd.example.todo.logic.TodoCoreLogic
import com.eiipii.esddd.example.todo.model._
import org.scalatest.FlatSpec

import scala.concurrent.duration._
import scalaz.{-\/, \/-}

/** Example of test for projection using strems
 */
class ListTodoProjectionTest extends FlatSpec {
  implicit val system = ActorSystem("testProjection")
  implicit val materializer = ActorMaterializer()

  it should "projects events on a stream" in {
    //Given
    val domain = ESDDD.domain
      .withDomainLogic(new TodoCoreLogic)
      .withBackend(InMemoryEventStoreBackend)
      .build()
    //and a list exists
    val listCreated = domain.commandExecution.execute(CreateToDoList("email:anyOwner"))
    val listAdded = listCreated match {
      case -\/(a) => fail(s"failed to create todo list ${a}")
      case \/-(b) => b.rootAggregate
    }
    val listID = listAdded match {
      case ToDoList(listID) => listID
    }
    val aggregateSource: Source[ToDoEvent, NotUsed] = EventStoreSource.aggregateEventsSource[TodoCoreModel](domain.eventStore, listAdded)
    val projFlow: Flow[ToDoEvent, ListTodoData, NotUsed] = EventStoreFlow.projectionFlow(new ListTodoProjection(listID))
    val graph: Source[ListTodoData, NotUsed] = aggregateSource.via(projFlow).log("projectionLog").withAttributes(Attributes.logLevels(onElement = Logging.DebugLevel))

    //When
    val probe: Probe[ListTodoData] = graph.runWith(TestSink.probe[ListTodoData])

    domain.commandExecution.execute(AddToDo(listID, s"text"))
    val expectedTodoID = TodoID(listID, 0)
    domain.commandExecution.execute(MarkAsUndone(listID, expectedTodoID))
    domain.commandExecution.execute(MarkAsDone(listID, expectedTodoID))
    domain.commandExecution.execute(UpdateToDo(listID, "updated", expectedTodoID))
    domain.commandExecution.execute(DeleteToDo(listID, expectedTodoID))

    //Then the expected projections a provided
    val moreThanNeeded = 10
    probe.request(moreThanNeeded)
      .expectNext(ListTodoData(listID, Vector())) // Zero value
      .expectNext(ListTodoData(listID, Vector())) // After list created
      .expectNext(ListTodoData(ListID(0), Vector(TodoData(TodoID(ListID(0), 0), "text", false, false))))
      .expectNext(ListTodoData(ListID(0), Vector(TodoData(TodoID(ListID(0), 0), "text", false, false))))
      .expectNext(ListTodoData(ListID(0), Vector(TodoData(TodoID(ListID(0), 0), "text", true, false))))
      .expectNext(ListTodoData(ListID(0), Vector(TodoData(TodoID(ListID(0), 0), "updated", true, false))))
      .expectNext(ListTodoData(ListID(0), Vector(TodoData(TodoID(ListID(0), 0), "updated", true, true))))
      .expectNoMessage(100.millis)
  }

}
