## Release notes

# 0.2.1

> 11 May 2017

## Added

-  Snapshot sources streams.


# 0.2.0

> 8 May 2017

A complete rewrite of the domain definition API and split of the backend to separate artefacts. A first implementation of reactive streams from the event store is provided in akka streams.

