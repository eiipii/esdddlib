
import com.typesafe.sbt.SbtPgp.autoImport._
import sbt.Keys._
import sbt._

object PublishSettings {

  val sonatypeSettings = Seq(
    useGpg := true,
    usePgpKeyHex("25AB1E10DB5E4DC393CD84133915B2FA12CBEB4C"),
    publishMavenStyle := true,
    publishArtifact in Test := false,
    pomIncludeRepository := {
      _ => false
    },
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value) {
        Some("snapshots" at nexus + "content/repositories/snapshots")
      } else {
        Some("releases" at nexus + "service/local/staging/deploy/maven2")
      }
    },
    useGpg := true,
    useGpgAgent := true,
    pomExtra := (
      <url>https://bitbucket.org/eiipii/esdddlib</url>
        <licenses>
          <license>
            <name>MIT License</name>
            <url>https://opensource.org/licenses/mit-license.php</url>
            <distribution>repo</distribution>
          </license>
        </licenses>
        <scm>
          <url>https://bitbucket.org/eiipii/esdddlib</url>
          <developerConnection>scm:git:git@bitbucket.org:eiipii/esdddlib.git</developerConnection>
          <connection>scm:git:git@bitbucket.org:eiipii/esdddlib.git</connection>
        </scm>
        <developers>
          <developer>
            <id>paweld2</id>
            <name>Paweł Cesar Sanjuan Szklarz</name>
            <url>http://eiipii.com</url>
          </developer>
        </developers>
      )
  )
}