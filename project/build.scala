/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

import sbt.Keys._
import sbt._

object esdddBuild extends Build {

  lazy val Organization = "com.eiipii"
  lazy val Version = "0.2.2-SNAPSHOT"

  lazy val eventSourcingApi = (project in file("eventSourcingApi")).
    settings(commonSettings: _*).
    settings(
      name := "event-sourcing-api"
    ).
    settings(apiDependencies: _*).
    settings(testDependenciesOnTest: _*)

  lazy val eventSourcingTest = (project in file("eventSourcingTest")).
    settings(commonSettings: _*).
    settings(
      name := "event-sourcing-test"
    ).
    settings(coreDependencies: _*).
    settings(testDependenciesOnMain: _*).
    dependsOn(eventSourcingApi)

  lazy val eventSourcing = (project in file("eventSourcing")).
    settings(commonSettings: _*).
    settings(
      name := "event-sourcing"
    ).
    settings(coreDependencies: _*).
    settings(testDependenciesOnTest: _*).
    dependsOn(eventSourcingTest % "test").
    dependsOn(eventSourcingApi)

  lazy val eventSourcingSql = (project in file("eventSourcingSql")).
    settings(commonSettings: _*).
    settings(
      name := "event-sourcing-sql"
    ).
    settings(sqlDependencies: _*).
    settings(testDependenciesOnTest: _*).
    dependsOn(eventSourcingTest % "test").
    dependsOn(eventSourcing)

  lazy val eventSourcingStream = (project in file("eventSourcingStream")).
    settings(commonSettings: _*).
    settings(
      name := "event-sourcing-stream"
    ).
    settings(streamDependencies: _*).
    settings(streamDependenciesForTests: _*).
    settings(testDependenciesOnTest: _*).
    dependsOn(eventSourcingTest % "test").
    dependsOn(eventSourcing)

  lazy val exampleDomain = (project in file("exampleDomain")).
    settings(
      publish := {},
      publishLocal := {},
      publishArtifact := false,
      publishTo := Some(Resolver.file("Unused transient repository", file("target/unusedrepo")))
    ).
    settings(commonSettings: _*).
    settings(
      name := "exampleDomain"
    ).
    settings(exampleDomainDependencies: _*).
    settings(streamDependenciesForTests: _*).
    dependsOn(eventSourcingTest % "test").
    dependsOn(eventSourcingStream).
    dependsOn(eventSourcing)

  lazy val eventSourcingRoot = (project in file(".")).
    settings(
      publish := {},
      publishLocal := {},
      publishArtifact := false,
      publishTo := Some(Resolver.file("Unused transient repository", file("target/unusedrepo")))
    ).
    aggregate(eventSourcing, eventSourcingApi, eventSourcingTest, eventSourcingStream, eventSourcingSql, exampleDomain)

  lazy val scalazVersion = "7.2.16"
  lazy val json4sVersion = "3.5.3"

  lazy val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"

  lazy val exampleDomainDependencies = Seq(
    libraryDependencies ++= Seq(
      "org.json4s" %% "json4s-native" % json4sVersion,
      "com.h2database" % "h2" % "1.4.196" % "test",
      logback % "test"
    ) ++ monocleLibs
  )
  lazy val apiDependencies = Seq(
    libraryDependencies ++= Seq(
      "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
      "org.scalaz" %% "scalaz-core" % scalazVersion,
      "joda-time" % "joda-time" % "2.9.9"
    )
  )

  val monocleVersion = "1.4.0"
  lazy val monocleLibs = Seq(
    "com.github.julien-truffaut" %% "monocle-core" % monocleVersion,
    "com.github.julien-truffaut" %% "monocle-generic" % monocleVersion,
    "com.github.julien-truffaut" %% "monocle-macro" % monocleVersion
  )


  lazy val coreDependencies = Seq(
    libraryDependencies ++= Seq(
      "org.scalaz" %% "scalaz-concurrent" % scalazVersion,
      "org.json4s" %% "json4s-native" % json4sVersion % "test"
    ) ++ monocleLibs.map(_ % "test")
  )

  lazy val akkaVersion = "2.5.6"
  lazy val streamDependencies = Seq(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % akkaVersion
    )
  )
  lazy val streamDependenciesForTests = Seq(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
    )
  )

  val scalikejdbcVersion = "2.5.2"
  lazy val sqlDependencies = Seq(
    libraryDependencies ++= Seq(
      "org.scalikejdbc" %% "scalikejdbc" % scalikejdbcVersion,
      "org.scalikejdbc" %% "scalikejdbc-config" % scalikejdbcVersion,
      "org.scalikejdbc" %% "scalikejdbc-test" % scalikejdbcVersion % "test",
      "com.h2database" % "h2" % "1.4.195" % "test",
      "mysql" % "mysql-connector-java" % "5.1.40" % "test",
      "org.json4s" %% "json4s-native" % json4sVersion % "test",
      "org.postgresql" % "postgresql" % "42.1.1" % "test",
      logback % "test"
    )
  )

  val testLibraries = Seq(
    "org.typelevel" %% "scalaz-scalatest" % "1.1.2",
    "org.scalatest" %% "scalatest" % "3.0.3",
    "org.scalacheck" %% "scalacheck" % "1.13.5",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0",
    "org.json4s" %% "json4s-native" % json4sVersion,
    "org.mockito" % "mockito-core" % "2.7.22"
  )

  lazy val testDependenciesOnMain = Seq(
    libraryDependencies ++= testLibraries
  )
  lazy val testDependenciesOnTest = Seq(
    libraryDependencies ++= testLibraries.map(_ % "test")
  )
  def nexusRepository(isSnapshot: Boolean): Resolver = {
    val nexus = "http://nexus.dev.eiipii.com:8081/"
    if (isSnapshot) {
      "snapshots" at nexus + "repository/maven-snapshots"
    } else {
      "releases" at nexus + "repository/maven-releases"
    }
  }

  lazy val commonSettings = Defaults.coreDefaultSettings ++ Seq(
    organization := Organization,
    version := Version,
    concurrentRestrictions in Global += Tags.limit(Tags.Test, 4),
    resolvers += Classpaths.typesafeReleases,
    publishArtifact in(Test, packageBin) := true,
    publishArtifact in(Test, packageDoc) := true,
    publishArtifact in(Test, packageSrc) := true,
    publishMavenStyle := true,
    publishM2Configuration := Classpaths.publishConfig(packagedArtifacts.in(publishM2).value, None,
      resolverName = nexusRepository(isSnapshot.value).name, checksums = checksums.in(publishM2).value,
      logging = ivyLoggingLevel.value, overwrite = isSnapshot.value),
    publishM2 <<= Classpaths.publishTask(publishM2Configuration, deliverLocal),
    otherResolvers += nexusRepository(isSnapshot.value),
    credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

  ) ++ Format.settings ++ PublishSettings.sonatypeSettings

}


object Format {

  import com.typesafe.sbt.SbtScalariform._

  lazy val settings = scalariformSettings ++ Seq(
    ScalariformKeys.preferences := formattingPreferences
  )

  lazy val formattingPreferences = {
    import scalariform.formatter.preferences._
    FormattingPreferences().
      setPreference(AlignParameters, true).
      setPreference(AlignSingleLineCaseStatements, true).
      setPreference(CompactControlReadability, true).
      setPreference(CompactStringConcatenation, true).
      setPreference(DoubleIndentClassDeclaration, true).
      setPreference(FormatXml, true).
      setPreference(IndentLocalDefs, true).
      setPreference(IndentPackageBlocks, true).
      setPreference(IndentSpaces, 2).
      setPreference(MultilineScaladocCommentsStartOnFirstLine, true).
      setPreference(PreserveSpaceBeforeArguments, false).
      setPreference(RewriteArrowSymbols, false).
      setPreference(SpaceBeforeColon, false).
      setPreference(SpaceInsideBrackets, false).
      setPreference(SpacesWithinPatternBinders, true)
  }
}
