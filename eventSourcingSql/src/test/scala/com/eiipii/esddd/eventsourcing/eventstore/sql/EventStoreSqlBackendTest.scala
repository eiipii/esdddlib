/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.sql

import com.eiipii.esddd.eventsourcing.eventstore._
import com.eiipii.esddd.test.UnitTest
import com.eiipii.esddd.test.eventstore.EventStoreWithVersionedEventStoreViewBehaviour
import com.eiipii.esddd.test.model.{TheTestDomainLogic, TheTestDomainModel, TheTestEventSerializationSchema}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.PropertyChecks
import org.scalatest.{AppendedClues, FlatSpec, Matchers}
import org.typelevel.scalatest.DisjunctionMatchers
import scalikejdbc._
import scalikejdbc.config._

class EventStoreSqlBackendTest extends FlatSpec with Matchers with PropertyChecks with ScalaFutures with DisjunctionMatchers with AppendedClues with EventStoreWithVersionedEventStoreViewBehaviour {

  GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(enabled = false, stackTraceDepth = 3)
  GlobalSettings.loggingSQLErrors = false
  DBs.setup('eventStoreTestDB2)
  DBs.setup('eventStoreTestDB2WithSerializableTransactionLevel)
  DBs.setup('eventStoreTestDB2ForReInitialization)

  def sqlH2BasedEventStoreWithProjection(): EventStore[TheTestDomainModel] = {
    val domainLogic = new TheTestDomainLogic()
    val serialization = new TheTestEventSerializationSchema()
    val eventStoreBackend = new EventStoreSqlBackend[TheTestDomainModel](
      EventStoreSqlConfiguration(EventStoreDatabaseConfig('eventStoreTestDB2, H2EventStoreSqlDialect, "testEventStore", rebuildDDL = true, ReadCommitted)),
      domainLogic,
      serialization
    )
    eventStoreBackend.initializeBackend()
    new AtomicEventStoreFromBackend(eventStoreBackend, domainLogic.equalityRelations)
  }

  def sqlH2BasedEventStoreWithProjectionWithSerializableTransactionLevel(): EventStore[TheTestDomainModel] = {
    val domainLogic = new TheTestDomainLogic()
    val serialization = new TheTestEventSerializationSchema()
    val eventStoreBackend = new EventStoreSqlBackend[TheTestDomainModel](
      EventStoreSqlConfiguration(EventStoreDatabaseConfig('eventStoreTestDB2WithSerializableTransactionLevel, H2EventStoreSqlDialect, "testEventStoreSerializable", rebuildDDL = true, Serializable)),
      domainLogic,
      serialization
    )
    eventStoreBackend.initializeBackend()
    new AtomicEventStoreFromBackend(eventStoreBackend, domainLogic.equalityRelations)
  }

  it should "not recreate the schema on a second reinitialization" in {
    //given
    val domainLogic = new TheTestDomainLogic()
    val serialization = new TheTestEventSerializationSchema()
    val eventStoreBackend = new EventStoreSqlBackend[TheTestDomainModel](
      EventStoreSqlConfiguration(EventStoreDatabaseConfig('eventStoreTestDB2ForReInitialization, H2EventStoreSqlDialect, "testReinitialization", rebuildDDL = false, Serializable)),
      domainLogic,
      serialization
    )
    //when
    val first = eventStoreBackend.initializeBackend()
    val second = eventStoreBackend.initializeBackend()

    //then
    first should be(true)
    second should be(false)
  }

  "A sql h2 based implementation of the event store " should behave like eventStoreWithAtomicProjection(UnitTest, sqlH2BasedEventStoreWithProjection _, isolationLevelIsSerializable = false)

  "A sql h2 based implementation with serializable transaction level of the event store " should behave like eventStoreWithAtomicProjection(
    UnitTest,
    sqlH2BasedEventStoreWithProjectionWithSerializableTransactionLevel _,
    isolationLevelIsSerializable = true
  )

}

