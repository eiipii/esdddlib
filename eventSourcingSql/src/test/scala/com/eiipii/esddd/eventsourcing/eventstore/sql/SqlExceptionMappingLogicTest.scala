/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.sql

import java.sql.SQLException

import com.eiipii.esddd.eventsourcing.eventstore._
import org.mockito.Mockito
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}

class SqlExceptionMappingLogicTest extends FlatSpec with Matchers with MockitoSugar {

  it should "map deadlock exceptions to EventSourceCommandScopeDeadLock" in {
    //given
    val sqlException = mock[SQLException]
    Mockito.when(sqlException.getSQLState).thenReturn("40001")
    val anyDefault = EventSourceCommandInternalError(new IllegalArgumentException())
    //when
    val mapped: EventSourceCommandFailure = new SqlExceptionMappingLogic {}.mapException(sqlException, Some(anyDefault))
    //then
    mapped should be(EventSourceCommandScopeDeadLock(sqlException))
  }

  it should "map constraints violation exceptions to the default provided" in {
    //given
    val sqlException = mock[SQLException]
    Mockito.when(sqlException.getSQLState).thenReturn("23505")
    val anyDefault = EventSourceCommandInternalError(new IllegalArgumentException())
    //when
    val mapped: EventSourceCommandFailure = new SqlExceptionMappingLogic {}.mapException(sqlException, Some(anyDefault))
    //then the default is used
    mapped should be(anyDefault)
  }

  it should "map constraints violation exceptions to internal error when default is not provided" in {
    //given
    val sqlException = mock[SQLException]
    Mockito.when(sqlException.getSQLState).thenReturn("23505")
    val noDefault = None
    //when
    val mapped: EventSourceCommandFailure = new SqlExceptionMappingLogic {}.mapException(sqlException, noDefault)
    //then the default is used
    mapped should be(EventSourceCommandInternalError(sqlException))
  }

  it should "map constraints violation exceptions to the default provided - Mysql case" in {
    //given
    val sqlException = mock[SQLException]
    Mockito.when(sqlException.getSQLState).thenReturn("23000")
    val anyDefault = EventSourceCommandInternalError(new IllegalArgumentException())
    //when
    val mapped: EventSourceCommandFailure = new SqlExceptionMappingLogic {}.mapException(sqlException, Some(anyDefault))
    //then the default is used
    mapped should be(anyDefault)
  }

  it should "map constraints violation exceptions to internal error when default is not provided - Mysql case" in {
    //given
    val sqlException = mock[SQLException]
    Mockito.when(sqlException.getSQLState).thenReturn("23000")
    val noDefault = None
    //when
    val mapped: EventSourceCommandFailure = new SqlExceptionMappingLogic {}.mapException(sqlException, noDefault)
    //then the default is used
    mapped should be(EventSourceCommandInternalError(sqlException))
  }

  it should "map all others exceptions to internal error" in {
    //given
    val sqlException = mock[SQLException]
    Mockito.when(sqlException.getSQLState).thenReturn("12312")
    val anyDefault = EventSourceCommandInternalError(new IllegalArgumentException())
    //when
    val mapped: EventSourceCommandFailure = new SqlExceptionMappingLogic {}.mapException(sqlException, Some(anyDefault))
    //then the internal error with the sql exception is provided
    mapped should be(EventSourceCommandInternalError(sqlException))
  }
}
