/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.sql

import java.util.concurrent.atomic.AtomicInteger

import com.eiipii.esddd.eventsourcing.eventstore.{AtomicTransactionScope, EventSerializationSchema}
import com.eiipii.esddd.eventsourcing.logic.{EventGroupProjection, StateAtomicProjectionPoint}
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}
import com.eiipii.esddd.eventsourcing.{EventStoreVersion, _}
import org.scalatest.{Matchers, fixture}
import scalikejdbc._
import scalikejdbc.config.DBs

import scalaz.\/-

class EventStoreSqlReadOnlyTransactionTest extends fixture.FlatSpec with DBAutoRollback with Matchers {

  GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(enabled = false, stackTraceDepth = 3)
  GlobalSettings.loggingSQLErrors = false

  DBs.setup('eventStoreTestDB)

  override def databaseInstance(): DB = NamedDB('eventStoreTestDB).toDB

  val databaseCounter = new AtomicInteger(0)

  it should "provide empty state on init" in { implicit db =>
    val ddl = createDDL()
    val atomicTransaction = eventStoreReadOnlyTransaction(db, ddl)
    atomicTransaction.eventStoreVersion shouldBe EventStoreVersion.zero
    atomicTransaction.projectionState.eventStoreVersion shouldBe EventStoreVersion.zero
    atomicTransaction.extractEventRange(EventStoreRange(EventStoreVersion.zero, None)) shouldBe empty
  }

  it should "provide correct event store version after persist" in { implicit db =>
    val ddl = createDDL()
    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val transactionScope = AtomicTransactionScope[SqlTestDomain](Map(SqlTestAggregate(0) -> 0L), Map(), SqlTestProjectionState(EventStoreVersion.zero))
    val version = atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), SqlTestAggregate(0), transactionScope)
    version match {
      case SqlTransactionWriteErrorResult(commandResult) => fail(s"not expected error $commandResult")
      case SqlUpdateStateInformation(cachedStartState, eventsFromStartToTransactionInitialState) =>
        cachedStartState.version.storeVersion should be(0L)
        eventsFromStartToTransactionInitialState shouldBe empty
    }
  }

  it should "not update projection or event store version after persist of events" in { implicit db =>
    val ddl = createDDL()
    val readTransaction = eventStoreReadOnlyTransaction(db, ddl)
    readTransaction.eventStoreVersion shouldBe EventStoreVersion.zero

    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val transactionScope = AtomicTransactionScope[SqlTestDomain](Map(SqlTestAggregate(0) -> 0L), Map(), SqlTestProjectionState(EventStoreVersion.zero))
    atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), SqlTestAggregate(0), transactionScope)
    atomicTransaction.eventStoreVersion shouldBe EventStoreVersion.zero
    //then
    readTransaction.projectionState.eventStoreVersion shouldBe EventStoreVersion.zero
  }

  it should "store events" in { implicit db =>
    val ddl = createDDL()
    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val transactionScope = AtomicTransactionScope[SqlTestDomain](Map(SqlTestAggregate(0) -> 0L), Map(), SqlTestProjectionState(EventStoreVersion.zero))
    atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), SqlTestAggregate(0), transactionScope)

    val readTransaction = eventStoreReadOnlyTransaction(db, ddl)
    readTransaction.extractEventRange(EventStoreRange(EventStoreVersion.zero, None)) should be(Seq(1, 2, 3))
  }

  it should "use event store version according to the transaction status" in { implicit db =>
    val ddl = createDDL()
    //given a transaction instance created events
    val readTransaction = eventStoreReadOnlyTransaction(db, ddl)
    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val transactionScope = AtomicTransactionScope[SqlTestDomain](Map(SqlTestAggregate(0) -> 0L), Map(), SqlTestProjectionState(EventStoreVersion.zero))
    atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), SqlTestAggregate(0), transactionScope)
    readTransaction.extractEventRange(EventStoreRange(EventStoreVersion.zero, None)) should be(Seq(1, 2, 3))
    //when next transaction is created
    val atomicTransaction2 = eventStoreReadOnlyTransaction(db, ddl)
    //then state match the changed values
    atomicTransaction2.eventStoreVersion shouldBe EventStoreVersion(3)
    atomicTransaction2.projectionState.eventStoreVersion shouldBe EventStoreVersion(3)
  }

  it should "calculate aggregate versions for a empty database" in { implicit db =>
    val ddl = createDDL()
    //given a transaction instance created events
    val atomicTransaction = eventStoreReadOnlyTransaction(db, ddl)
    //when
    val aggregatesVersion = atomicTransaction.calculateAggregateVersions(Set(SqlTestAggregate(0)))
    //then
    aggregatesVersion should be(\/-(Map(SqlTestAggregate(0) -> 0L)))
  }
  it should "calculate constraint versions for a empty database" in { implicit db =>
    val ddl = createDDL()
    //given a transaction instance created events
    val atomicTransaction = eventStoreReadOnlyTransaction(db, ddl)
    //when
    val constraintVersion = atomicTransaction.calculateConstraintVersions(Set(SqlConstraintScope(0)))
    //then
    constraintVersion should be(\/-(Map(SqlConstraintScope(0) -> 0L)))
  }

  it should "calculate correct transaction scopes versions - aggregate case" in { implicit db =>
    val aggregateScope: Set[SqlTestAggregate] = Set(SqlTestAggregate(0))
    val constraintScope: Set[SqlConstraintScope] = Set()
    val rootAggregate: SqlTestAggregate = SqlTestAggregate(0)
    val expectedAggregateVersions: Map[SqlTestAggregate, Long] = Map(SqlTestAggregate(0) -> 1L)
    val expectedConstraintVersions: Map[SqlConstraintScope, Long] = Map()
    testScopeTransaction(aggregateScope, constraintScope, rootAggregate, expectedAggregateVersions, expectedConstraintVersions)
  }

  it should "calculate correct transaction scopes versions - constraint case" in { implicit db =>
    val aggregateScope: Set[SqlTestAggregate] = Set()
    val constraintScope: Set[SqlConstraintScope] = Set(SqlConstraintScope(0))
    val rootAggregate: SqlTestAggregate = SqlTestAggregate(0)
    val expectedAggregateVersions: Map[SqlTestAggregate, Long] = Map()
    val expectedConstraintVersions: Map[SqlConstraintScope, Long] = Map(SqlConstraintScope(0) -> 1L)
    testScopeTransaction(aggregateScope, constraintScope, rootAggregate, expectedAggregateVersions, expectedConstraintVersions)
  }

  it should "calculate correct transaction scopes versions - mixed case" in { implicit db =>
    val aggregateScope: Set[SqlTestAggregate] = Set(SqlTestAggregate(0))
    val constraintScope: Set[SqlConstraintScope] = Set(SqlConstraintScope(0))
    val rootAggregate: SqlTestAggregate = SqlTestAggregate(0)
    val expectedAggregateVersions: Map[SqlTestAggregate, Long] = Map(SqlTestAggregate(0) -> 1L)
    val expectedConstraintVersions: Map[SqlConstraintScope, Long] = Map(SqlConstraintScope(0) -> 1L)
    testScopeTransaction(aggregateScope, constraintScope, rootAggregate, expectedAggregateVersions, expectedConstraintVersions)
  }

  it should "rollback transactions when constrain version do not match" in { implicit db =>
    val ddl = createDDL()
    //given a transaction instance created events
    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val readTransaction = eventStoreReadOnlyTransaction(db, ddl)
    //and an a constraint is used to execute a transaction
    val aggregatesVersion = readTransaction.calculateAggregateVersions(Set()).toOption.get
    val constraintsVersion = readTransaction.calculateConstraintVersions(Set(SqlConstraintScope(0))).toOption.get
    val transactionScope = AtomicTransactionScope[SqlTestDomain](aggregatesVersion, constraintsVersion, SqlTestProjectionState(EventStoreVersion.zero))
    atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), SqlTestAggregate(0), transactionScope)
    //when the same version is used on a transaction for other aggregate

    val atomicTransaction2 = eventStoreWriteTransaction(db, ddl)
    val secondTransactionResult = atomicTransaction2.persistEventsOnAtomicTransaction(List(4), SqlTestAggregate(1), transactionScope)
    //then a exception is returned
    secondTransactionResult match {
      case SqlTransactionWriteErrorResult(commandResult)                                         => commandResult.isLeft should be(true)
      case SqlUpdateStateInformation(cachedStartState, eventsFromStartToTransactionInitialState) => fail("Transaction should rollback because of version")
    }
  }

  it should "rollback transactions on already used transactions scopes" in { implicit db =>
    val ddl = createDDL()
    //given a transaction instance created events
    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val readTransaction = eventStoreReadOnlyTransaction(db, ddl)
    //and an aggregate version is used to store values
    val aggregatesVersion = readTransaction.calculateAggregateVersions(Set(SqlTestAggregate(0))).toOption.get
    val transactionScope = AtomicTransactionScope[SqlTestDomain](aggregatesVersion, Map(), SqlTestProjectionState(EventStoreVersion.zero))
    atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), SqlTestAggregate(0), transactionScope)
    //when the same version is used in other transaction

    val atomicTransaction2 = eventStoreWriteTransaction(db, ddl)
    val secondTransactionResult = atomicTransaction2.persistEventsOnAtomicTransaction(List(4), SqlTestAggregate(0), transactionScope)
    //then a exception is returned
    secondTransactionResult match {
      case SqlTransactionWriteErrorResult(commandResult)                                         => commandResult.isLeft should be(true)
      case SqlUpdateStateInformation(cachedStartState, eventsFromStartToTransactionInitialState) => fail("Transaction should rollback because of version")
    }
  }

  def testScopeTransaction(
    aggregateScope:             Set[SqlTestAggregate],
    constraintScope:            Set[SqlConstraintScope],
    rootAggregate:              SqlTestAggregate,
    expectedAggregateVersions:  Map[SqlTestAggregate, Long],
    expectedConstraintVersions: Map[SqlConstraintScope, Long]
  )(implicit db: DB): Any = {
    val ddl = createDDL()
    //given a transaction instance created events
    val atomicTransaction = eventStoreWriteTransaction(db, ddl)
    val readTransaction = eventStoreReadOnlyTransaction(db, ddl)
    //and the current aggregate version is extracted
    val aggregatesVersion = readTransaction.calculateAggregateVersions(aggregateScope).toOption.get
    val constraintVersion = readTransaction.calculateConstraintVersions(constraintScope).toOption.get
    val transactionScope = AtomicTransactionScope[SqlTestDomain](aggregatesVersion, constraintVersion, SqlTestProjectionState(EventStoreVersion.zero))
    //when events are stored
    atomicTransaction.persistEventsOnAtomicTransaction(List(1, 2, 3), rootAggregate, transactionScope)
    //then the aggregate version change on the next transaction
    val atomicTransaction2 = eventStoreReadOnlyTransaction(db, ddl)
    val aggregatesVersionAfter = atomicTransaction2.calculateAggregateVersions(aggregateScope).toOption.get
    val constraintVersionAfter = atomicTransaction2.calculateConstraintVersions(constraintScope).toOption.get
    aggregatesVersionAfter should be(expectedAggregateVersions)
    constraintVersionAfter should be(expectedConstraintVersions)
  }

  def createDDL(): EventStoreSqlDDL = {
    val ddl = EventStoreSqlDDL.fromDialect(H2EventStoreSqlDialect, s"test${databaseCounter.getAndIncrement()}")
    ddl.createTables(databaseInstance())
    ddl
  }

  def eventStoreReadOnlyTransaction(db: DB, ddl: EventStoreSqlDDL): EventStoreSqlReadOnlyReadTransaction[SqlTestDomain] =
    new EventStoreSqlReadOnlyReadTransaction[SqlTestDomain](
      db,
      new InternalCacheStateVersion(internalStateZero),
      new SqlTestEventSerializationSchema(),
      new SqlTestEqualityRelations(),
      ddl
    )

  val internalStateZero = AtomicEventStoreStateSql[SqlTestDomain](EventStoreVersion.zero, new StateAtomicProjectionPoint(new SqlTestGroup()))

  def eventStoreWriteTransaction(db: DB, ddl: EventStoreSqlDDL): EventStoreSqlWriteTransaction[SqlTestDomain] = {
    new EventStoreSqlWriteTransaction[SqlTestDomain](db, new SqlTestEventSerializationSchema(), new SqlTestEqualityRelations(), ddl, new InternalCacheStateVersion(internalStateZero))
  }

}

trait SqlTestDomain extends DomainCoreModel {
  type Command = Any
  type Event = Int
  type Aggregate = SqlTestAggregate
  type ConstraintScope = SqlConstraintScope
  type State = SqlTestProjection
}

trait SqlTestProjection {
  def eventStoreVersion: EventStoreVersion
}

case class SqlTestAggregate(scope: Int)

case class SqlConstraintScope(scope: Int)

case class SqlTestProjectionState(eventStoreVersion: EventStoreVersion) extends SqlTestProjection

class SqlTestEventSerializationSchema extends EventSerializationSchema[SqlTestDomain] {

  override def eventToData(event: Int): EventData = EventData(Array(event.toByte))

  override def mapToEvent(data: EventData): Int = data.eventBytes(0).toInt
}

class SqlTestEqualityRelations extends DomainEqualityRelations[SqlTestDomain] {
  override def buildAggregateReference(aggregate: SqlTestAggregate): AggregateReference =
    AggregateReference(0, aggregate.scope)

  override def buildConstraintReference(constraintScope: SqlConstraintScope): Option[ConstraintReference] =
    Some(ConstraintReference(0, constraintScope.scope))
}

class SqlTestGroup extends EventGroupProjection[SqlTestDomain, SqlTestProjectionState] {
  override lazy val zero: SqlTestProjectionState = SqlTestProjectionState(EventStoreVersion.zero)

  override def projectSingleEvent(state: SqlTestProjectionState, event: Int): SqlTestProjectionState =
    SqlTestProjectionState(state.eventStoreVersion.add(1))
}
