/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.sql

import com.eiipii.esddd.eventsourcing.eventstore.{AtomicEventStoreFromBackend, EventStore}
import com.eiipii.esddd.test.IntegrationTest
import com.eiipii.esddd.test.eventstore.EventStoreWithVersionedEventStoreViewBehaviour
import com.eiipii.esddd.test.model.{TheTestDomainLogic, TheTestDomainModel, TheTestEventSerializationSchema}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.PropertyChecks
import org.scalatest.{AppendedClues, FlatSpec, Matchers}
import org.typelevel.scalatest.DisjunctionMatchers
import scalikejdbc.config.DBs
import scalikejdbc.{GlobalSettings, LoggingSQLAndTimeSettings}

class EventStoreSqlBackendIntegrationTest extends FlatSpec with Matchers with PropertyChecks with ScalaFutures with DisjunctionMatchers with AppendedClues with EventStoreWithVersionedEventStoreViewBehaviour {

  GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(enabled = false, stackTraceDepth = 3)
  GlobalSettings.loggingSQLErrors = false
  DBs.setupAll()

  def sqlPostgresBasedEventStoreWithProjection(): EventStore[TheTestDomainModel] = {
    val domainLogic = new TheTestDomainLogic()
    val serialization = new TheTestEventSerializationSchema()
    val eventStoreBackend = new EventStoreSqlBackend[TheTestDomainModel](
      EventStoreSqlConfiguration(EventStoreDatabaseConfig('PostgresIntegration, PostgresEventStoreSqlDialect, "testEventStore", rebuildDDL = true, ReadCommitted)),
      domainLogic,
      serialization
    )
    eventStoreBackend.initializeBackend()
    new AtomicEventStoreFromBackend(eventStoreBackend, domainLogic.equalityRelations)
  }

  def sqlMySqlBasedEventStoreWithProjectionWithSerializableTransactionLevel(): EventStore[TheTestDomainModel] = {
    val domainLogic = new TheTestDomainLogic()
    val serialization = new TheTestEventSerializationSchema()
    val eventStoreBackend = new EventStoreSqlBackend[TheTestDomainModel](
      EventStoreSqlConfiguration(EventStoreDatabaseConfig('MySQLIntegration, MySqlEventStoreSqlDialect, "testEventStoreSerializable", rebuildDDL = true, ReadCommitted)),
      domainLogic,
      serialization
    )
    eventStoreBackend.initializeBackend()
    new AtomicEventStoreFromBackend(eventStoreBackend, domainLogic.equalityRelations)
  }

  "A sql postgresql based implementation of the event store " should behave like eventStoreWithAtomicProjection(IntegrationTest, sqlPostgresBasedEventStoreWithProjection _, isolationLevelIsSerializable = false)

  "A sql mysql based implementation with serializable transaction level of the event store " should behave like eventStoreWithAtomicProjection(
    IntegrationTest,
    sqlMySqlBasedEventStoreWithProjectionWithSerializableTransactionLevel _,
    isolationLevelIsSerializable = false
  )

}

