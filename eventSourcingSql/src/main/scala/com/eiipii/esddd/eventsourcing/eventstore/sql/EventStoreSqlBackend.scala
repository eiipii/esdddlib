/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.sql

import java.sql.SQLException

import com.eiipii.esddd.eventsourcing._
import com.eiipii.esddd.eventsourcing.eventstore._
import com.eiipii.esddd.eventsourcing.logic.{DomainCoreLogic, EventStoreAtomicProjectionPoint, FinalCommandExecutionResult}
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}
import scalikejdbc._

import scala.collection.concurrent.TrieMap
import scala.collection.immutable.Seq
import scalaz.{-\/, \/, \/-}

class EventStoreSqlBackend[D <: DomainCoreModel](
    val sqlConfiguration:    EventStoreSqlConfiguration,
    val logic:               DomainCoreLogic[D],
    val serializationSchema: EventSerializationSchema[D]
) extends EventStoreTransactionalBackend[D] with LoanPattern {

  private[this] val ddl = EventStoreSqlDDL.fromDialect(sqlConfiguration.dbConfig.dialect, sqlConfiguration.dbConfig.tablesNamespace)
  private[this] val connectionPool = ConnectionPool(sqlConfiguration.dbConfig.connectionPoolSymbol)
  private[this] val cacheRef = new InternalCacheStateVersion[D](AtomicEventStoreStateSql[D](EventStoreVersion.zero, logic.atomicProjectionProjection.zero))

  def initializeBackend(): Boolean = {
    withDb { db =>
      val exist = ddl.tablesExists(db)
      val drop = sqlConfiguration.dbConfig.rebuildDDL && exist
      val create = drop || !exist
      if (drop) {
        ddl.dropTables(db)
      }
      if (create) {
        ddl.createTables(db)
      }
      create
    }
  }

  private def withDb[A](f: DB => A): A = {
    using(connectionPool.borrow()) { conn: java.sql.Connection =>
      val db: DB = DB(conn)
      db.autoClose(false)
      f(db)
    }
  }

  override def readOnly[A](execution: (EventStoreReadTransaction[D]) => A): A = {
    withDbTransaction { db =>
      execution(new EventStoreSqlReadOnlyReadTransaction(db, cacheRef, serializationSchema, logic.equalityRelations, ddl))
    }
  }

  private def withDbTransaction[A](f: DB => A): A = {
    using(connectionPool.borrow()) { conn: java.sql.Connection =>
      conn.setTransactionIsolation(sqlConfiguration.dbConfig.transactionIsolationLevel.jdbcValue)
      val db: DB = DB(conn)
      db.begin()
      db.autoClose(false)
      try {
        f(db)
      }
      finally {
        db.commit()
      }
    }
  }

  override def persistEventsOnAtomicTransaction(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): DomainCommandResult[D] = {
    val result: SqlTransactionWriteResult[D] = withDbTransaction { db =>
      new EventStoreSqlWriteTransaction(db, serializationSchema, logic.equalityRelations, ddl, cacheRef).persistEventsOnAtomicTransaction(events, rootAggregate, atomicTransactionScope)
    }
    result match {
      case SqlTransactionWriteErrorResult(commandResult) => commandResult
      case SqlUpdateStateInformation(cachedStartState, eventsFromStartToTransactionInitialState) =>
        // The cachedStartState is a previously calculated state on this block of code
        // The eventsFromStartToTransactionInitialState is a list of events between the Start version and the "select max" result on the transaction.
        // First calculate the state on base of data read directly from the database, it means that the events order and version came from commit-read data
        val eventsFromDatabase = eventsFromStartToTransactionInitialState.map(eventDb => serializationSchema.mapToEvent(eventDb.eventData))
        val transactionInitialState = (cachedStartState.groupElement /: eventsFromDatabase) {
          case (groupElement, event) => groupElement.projectEvent(event)
        }
        val transactionInitialVersion = cachedStartState.version.add(eventsFromDatabase.size)
        cacheRef.updateCache(AtomicEventStoreStateSql[D](transactionInitialVersion, transactionInitialState))
        // Now calculate the state related to the command execution.
        val commandFinalState = (transactionInitialState /: events) {
          case (groupElement, event) => groupElement.projectEvent(event)
        }
        \/-(
          FinalCommandExecutionResult(
            rootAggregate,
            commandFinalState.state,
            events
          )
        )
    }
  }
}

trait VersionedSqlTransaction {
  def db: DB

  def ddlDialect: EventStoreSqlDDL

  lazy val eventStoreVersion: EventStoreVersion = {
    db.withinTx { implicit session =>
      val version: Option[Long] = ddlDialect.createSelectEventStoreVersion().apply().flatten
      version.map(v => EventStoreVersion(v)).getOrElse(EventStoreVersion.zero)
    }
  }

}

trait SqlExceptionMappingLogic {

  val sqlStateDeadLockState = "40001"
  val sqlStateDuplicateKeyViolationState = "23505"
  val sqlStateDuplicateKeyViolationStateMySQL = "23000"

  //TODO make cache optional. Is really necessary??
  def mapException(sqlException: SQLException, duplicatedKeyCase: Option[EventSourceCommandFailure] = None): EventSourceCommandFailure = {
    val sqlState: String = sqlException.getSQLState
    if (sqlState.compareTo(sqlStateDeadLockState) == 0) {
      EventSourceCommandScopeDeadLock(sqlException)
    }
    else if (sqlState.compareTo(sqlStateDuplicateKeyViolationState) == 0 || sqlState.compareTo(sqlStateDuplicateKeyViolationStateMySQL) == 0) {
      duplicatedKeyCase.getOrElse(EventSourceCommandInternalError(sqlException))
    }
    else {
      EventSourceCommandInternalError(sqlException)
    }
  }

}

case class AtomicEventStoreStateSql[D <: DomainCoreModel](version: EventStoreVersion, groupElement: EventStoreAtomicProjectionPoint[D])

class InternalCacheStateVersion[D <: DomainCoreModel](zero: AtomicEventStoreStateSql[D]) {

  val maxNumberOfOldVersions = 4
  val maxVersionDelay = 20
  private[this] val cache: TrieMap[EventStoreVersion, AtomicEventStoreStateSql[D]] = TrieMap.empty

  cache.put(EventStoreVersion.zero, zero)

  def findMatchingVersion(targetVersion: EventStoreVersion): AtomicEventStoreStateSql[D] =
    cache
      .filter { case (version, _) => version.storeVersion <= targetVersion.storeVersion }
      .maxBy { case (version, _) => version.storeVersion }
      ._2

  def updateCache(newStateSql: AtomicEventStoreStateSql[D]): Unit = {
    cache.putIfAbsent(newStateSql.version, newStateSql) match {
      case Some(oldValue) => //A old value that must match the version calculation TODO: logging
      case None           => //Ok, version on cache
    }
    if (cache.size > maxNumberOfOldVersions) {
      cache.keys
        .filter(key => key.storeVersion != 0 && key.storeVersion < newStateSql.version.storeVersion - maxVersionDelay)
        .take(maxNumberOfOldVersions / 2)
        .foreach { oldKey => cache.remove(oldKey) }
    }
  }

}

sealed trait SqlTransactionWriteResult[D <: DomainCoreModel]

case class SqlTransactionWriteErrorResult[D <: DomainCoreModel](commandResult: DomainCommandResult[D]) extends SqlTransactionWriteResult[D]

case class SqlUpdateStateInformation[D <: DomainCoreModel](
  cachedStartState:                         AtomicEventStoreStateSql[D],
  eventsFromStartToTransactionInitialState: List[EventDataWithNr]
) extends SqlTransactionWriteResult[D]

class EventStoreSqlWriteTransaction[D <: DomainCoreModel](
    val db:                DB,
    val schema:            EventSerializationSchema[D],
    val equalityRelations: DomainEqualityRelations[D],
    val ddlDialect:        EventStoreSqlDDL,
    val cacheRef:          InternalCacheStateVersion[D]
) extends VersionedSqlTransaction with PersistEventCommonLogic[D] with SqlExceptionMappingLogic {

  def persistEventsOnAtomicTransaction(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): SqlTransactionWriteResult[D] =
    internalPersistEventsOnAtomicTransaction(createPersistData(events, rootAggregate, atomicTransactionScope))

  def internalPersistEventsOnAtomicTransaction(data: EventPersistOperationData[D]): SqlTransactionWriteResult[D] = {
    val initialVersion: EventStoreVersion = eventStoreVersion
    db.withinTx { implicit session =>
      try {
        val stateToStart: AtomicEventStoreStateSql[D] = cacheRef.findMatchingVersion(initialVersion)
        // Inserts. Will fail if aggregates or constraints version do not match
        data.constraints.foreach {
          case (constraintRef, version) => ddlDialect.createConstraintInsert(constraintRef, version + 1).apply()
        }
        data.aggregates.foreach {
          case (aggregateRef, version) => ddlDialect.createAggregateInsert(aggregateRef, version + 1).apply()
        }
        (stateToStart.version.storeVersion /: data.events) {
          case (_, event) =>
            val eventData = schema.eventToData(event)
            ddlDialect.createEventInsert(eventData, data.rootAggregateRef).apply()
        }
        //Read all events between the provided start state and the transaction initial state
        val additionalEvents: List[EventDataWithNr] = if (stateToStart.version.storeVersion < initialVersion.storeVersion) {
          ddlDialect.createSelectEventFromTo(stateToStart.version.storeVersion + 1, initialVersion.storeVersion).apply()
        }
        else {
          List()
        }
        SqlUpdateStateInformation(stateToStart, additionalEvents)
      }
      catch {
        case sqlException: SQLException =>
          val rollbackCase = EventSourceCommandRollback(data.atomicTransactionScope.aggregateVersion, data.atomicTransactionScope.constraintScopeVersion)
          SqlTransactionWriteErrorResult(-\/(mapException(sqlException, Some(rollbackCase))))
      }
    }
  }
}

private class EventStoreAtomicProjectionBuilder[D <: DomainCoreModel](val cacheRef: InternalCacheStateVersion[D]) {

  def buildState(eventStoreVersion: EventStoreVersion, rangeExtractor: EventStoreRange => Seq[D#Event]): EventStoreAtomicProjectionPoint[D] = {
    val cachedVersion = cacheRef.findMatchingVersion(eventStoreVersion)
    if (cachedVersion.version.storeVersion == eventStoreVersion.storeVersion) {
      cachedVersion.groupElement
    }
    else {
      val range = EventStoreRange(cachedVersion.version.add(1), Some(eventStoreVersion.add(1)))
      val events: Seq[D#Event] = rangeExtractor(range)
      val finalState: EventStoreAtomicProjectionPoint[D] = (cachedVersion.groupElement /: events) {
        case (groupElement, event) =>
          groupElement.projectEvent(event)
      }
      finalState

    }
  }
}

class EventStoreSqlReadOnlyReadTransaction[D <: DomainCoreModel](
    val db:               DB,
    val cacheRef:         InternalCacheStateVersion[D],
    val schema:           EventSerializationSchema[D],
    val equalityRelation: DomainEqualityRelations[D],
    val ddlDialect:       EventStoreSqlDDL
) extends EventStoreReadTransaction[D] with VersionedSqlTransaction with SqlExceptionMappingLogic {

  override lazy val projectionState: D#State = new EventStoreAtomicProjectionBuilder(cacheRef).buildState(eventStoreVersion, extractEventRange).state

  def extractEventRange(range: EventStoreRange): Seq[D#Event] = {
    db.withinTx { implicit session =>
      val from = range.from.storeVersion
      val listEvent: List[EventDataWithNr] = range.to match {
        case Some(toVersion) =>
          ddlDialect.createSelectEventFromTo(from, toVersion.storeVersion).apply()
        case None =>
          if (range.from.storeVersion == 0L) {
            ddlDialect.createSelectAllEvents().apply()
          }
          else {
            ddlDialect.createSelectEventFrom(from).apply()
          }
      }
      listEvent.map { eventWithNr =>
        schema.mapToEvent(eventWithNr.eventData)
      }.toStream
    }
  }

  override def calculateAggregateVersions(aggregates: Set[D#Aggregate]): EventSourceCommandFailure \/ Map[D#Aggregate, Long] = db.withinTx { implicit session =>
      def extractAggregateVersion(aggregateRef: AggregateReference): Long = {
        val version: Option[Long] = ddlDialect.createSelectAggregateVersion(aggregateRef).apply().flatten
        version.getOrElse(0L)
      }

    try {
      val aggregateToVersion: Map[D#Aggregate, Long] = aggregates.map { aggregate =>
        aggregate -> extractAggregateVersion(equalityRelation.buildAggregateReference(aggregate))
      }(collection.breakOut)
      \/-(aggregateToVersion)
    }
    catch {
      case sqlException: SQLException =>
        -\/(mapException(sqlException))
    }
  }

  override def calculateConstraintVersions(constraints: Set[D#ConstraintScope]): EventSourceCommandFailure \/ Map[D#ConstraintScope, Long] = db.withinTx { implicit session =>
      def extractConstraintVersion(constraintRef: ConstraintReference): Long = {
        val version: Option[Long] = ddlDialect.createSelectConstraintVersion(constraintRef).apply().flatten
        version.getOrElse(0L)
      }

    try {
      val constraintToVersion: Map[D#ConstraintScope, Long] = constraints.map { constraint =>
        constraint -> extractConstraintVersion(equalityRelation.buildConstraintReference(constraint).get)
      }(collection.breakOut)
      \/-(constraintToVersion)
    }
    catch {
      case sqlException: SQLException =>
        -\/(mapException(sqlException))
    }
  }

  override def loadEventsForAggregate(aggregate: D#Aggregate, range: EventStoreRange, nrOfEvents: Int): AggregateEventRangeView[D] = {
    val aggregateRef = equalityRelation.buildAggregateReference(aggregate)
    db.withinTx { implicit session =>
      val listEvent: List[EventDataWithNr] = range.to match {
        case Some(to) => ddlDialect.createSelectAggregateEventsWithLimit(aggregateRef, range.from.storeVersion, to.storeVersion, nrOfEvents + 1).apply()
        case None     => ddlDialect.createSelectAggregateEvents(aggregateRef, range.from.storeVersion, nrOfEvents + 1).apply()
      }
      val events: Vector[EventReference[D]] = listEvent.take(nrOfEvents).map { eventWithNr =>
        EventReference[D](
          schema.mapToEvent(eventWithNr.eventData),
          EventStoreVersion(eventWithNr.eventNr)
        )
      }.toVector
      AggregateEventRangeView[D](events, listEvent.size < nrOfEvents + 1)
    }
  }

  override def calculateEventStoreVersion(): EventStoreVersion =
    db.withinTx { implicit session =>
      val version: Option[Long] = ddlDialect.createSelectEventStoreVersion().apply().flatten
      version match {
        case Some(value) => EventStoreVersion(value)
        case None        => EventStoreVersion.zero
      }
    }
}