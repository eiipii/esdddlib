#!/bin/bash

./eventSourcing/src/docker/startTestDatabases.sh

sbt clean coverage test -jvm-debug 8000

sbt coverageReport

sbt coverageAggregate

./eventSourcing/src/docker/stopTestDatabases.sh
