/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore

import com.eiipii.esddd.eventsourcing._
import org.scalacheck._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}
import Arbitrary.arbitrary
import org.scalactic.anyvals.PosInt

class EventStoreRangeUtils$Test extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {
  val nrOfTestEvents = 20
  val testList: Vector[Int] = (1 to nrOfTestEvents).toVector

  val maxVectorSizeForTest = 30
  val maxRangeLimit = 30
  val minSuccessfulForRange = 300

  implicit val rangeGeneratorDrivenConfig = PropertyCheckConfiguration(PosInt.from(minSuccessfulForRange).get)

  def rangeGen: Gen[EventStoreRange] = for {
    from <- Gen.choose[Int](0, maxRangeLimit)
    open <- arbitrary[Boolean]
    to <- Gen.choose(from + 1, maxRangeLimit + 1)
  } yield if (open) EventStoreRange(EventStoreVersion(from)) else EventStoreRange(EventStoreVersion(from), Some(EventStoreVersion(to)))

  it should "extract open ranges correctly" in {

    EventStoreRangeUtils.extractRangeFromVector(testList, EventStoreRange(EventStoreVersion.zero, None)) should be(testList)
    EventStoreRangeUtils.extractRangeFromVector(testList, EventStoreRange(EventStoreVersion(1L), None)) should be(testList)

    (2 to (nrOfTestEvents + 2)).foreach { from =>
      val extracted = EventStoreRangeUtils.extractRangeFromVector(testList, EventStoreRange(EventStoreVersion(from), None))
      extracted should be(testList.drop(from - 1))
    }

    EventStoreRange(EventStoreVersion(0), Some(EventStoreVersion(3)))
  }

  it should "extract correctly for" in {
    val longer = 21
    testExtract(longer, EventStoreRange(EventStoreVersion(0), Some(EventStoreVersion(3))))
    testExtract(2, EventStoreRange(EventStoreVersion(0), Some(EventStoreVersion(3))))

    testExtract(longer, EventStoreRange(EventStoreVersion(1), Some(EventStoreVersion(3))))
    testExtract(2, EventStoreRange(EventStoreVersion(1), Some(EventStoreVersion(3))))

    testExtract(longer, EventStoreRange(EventStoreVersion(0), Some(EventStoreVersion(1))))
    testExtract(1, EventStoreRange(EventStoreVersion(0), Some(EventStoreVersion(1))))
  }

  def testExtract(vectorSize: Int, range: EventStoreRange): Unit = {
    val testVector: Vector[Int] = (1 to vectorSize).toVector
    val expectedResult = range.to match {
      case Some(to) => testVector.filter(nr => nr >= range.from.storeVersion && nr < to.storeVersion)
      case None     => testVector.filter(_ >= range.from.storeVersion)
    }
    EventStoreRangeUtils.extractRangeFromVector(testVector, range) should be(expectedResult)
  }

  it should "extract close ranges correctly" in {
    forAll(
      Gen.choose(0, maxVectorSizeForTest), rangeGen
    ) { (vectorSize, range) => testExtract(vectorSize, range)
      }
  }
}
