/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.test.model.test

import com.eiipii.esddd.eventsourcing.DefaultInMemoryEventCommandExecutionForDomainLogic
import com.eiipii.esddd.eventsourcing.eventstore.EventSerializationSchema
import com.eiipii.esddd.eventsourcing.logic.DomainCoreLogic
import com.eiipii.esddd.eventsourcing.projections.EventsProjection
import com.eiipii.esddd.test.model._
import com.eiipii.esddd.test._

class TheTestDomainModuleTest extends AbstractDomainCoreTest[TheTestDomainModel, TestStateInMemory]
    with DefaultInMemoryEventCommandExecutionForDomainLogic
    with SchemaValidationTest[TheTestDomainModel]
    with StandardCommandHistoryTest[TheTestDomainModel, TestStateInMemory] {
  //Login to test
  override def logic(): DomainCoreLogic[TheTestDomainModel] = new TheTestDomainLogic()

  override def projection(): EventsProjection[TheTestDomainModel, TestStateInMemory] = new TheEventStateProjection()

  override def commandGenerator(): DomainCoreCommandGenerator[TheTestDomainModel, TestStateInMemory] = new TheTestDomainGenerator()

  override def validation: DomainCoreModelValidation[TheTestDomainModel, TestStateInMemory] =
    new DomainCoreModelValidation[TheTestDomainModel, TestStateInMemory] {
      override def postCommandValidation(
        command:       TheTestCommand,
        rootAggregate: TheTestAggregate,
        state:         TheTestState,
        projection:    TestStateInMemory
      ): Unit =
        command match {
          case TestCommandForThreads(threadNr, targetAggregate, constraint) => ()
          case TestCommandOne() => state.lastAdded() should be(1)
          case TestCommandTwo(createTwo) => if (createTwo) {
            state.lastAdded() should be(2)
          }
          else {
            state.lastAdded() should be(3)
          }
        }

      override def validateState(state: TheTestState, projection: TestStateInMemory): Unit = {
        state.history().split(',').count(_.compareTo("1") == 0) should be(state.countOne())
        state.history().split(',').count(_.compareTo("2") == 0) should be(state.countTwo())
        state.history().split(',').count(_.compareTo("3") == 0) should be(state.countThree())
      }
    }

  //schema
  override lazy val schema: EventSerializationSchema[TheTestDomainModel] = new TheTestEventSerializationSchema
}
