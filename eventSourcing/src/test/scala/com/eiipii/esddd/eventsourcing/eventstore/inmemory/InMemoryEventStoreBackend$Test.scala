/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.inmemory

import com.eiipii.esddd.eventsourcing.eventstore.{AtomicEventStoreFromBackend, EventStore}
import com.eiipii.esddd.test.UnitTest
import com.eiipii.esddd.test.eventstore.EventStoreWithVersionedEventStoreViewBehaviour
import com.eiipii.esddd.test.model.{TheTestDomainLogic, TheTestDomainModel}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.PropertyChecks
import org.scalatest.{AppendedClues, FlatSpec, FunSuite, Matchers}
import org.typelevel.scalatest.DisjunctionMatchers

class InMemoryEventStoreBackend$Test extends FlatSpec with Matchers with PropertyChecks with ScalaFutures with DisjunctionMatchers with AppendedClues with EventStoreWithVersionedEventStoreViewBehaviour {

  def inMemoryEventStoreWithProjection(): EventStore[TheTestDomainModel] = {
    val domainLogic = new TheTestDomainLogic()
    val eventStoreBackend = InMemoryEventStoreBackend.buildEventStoreBackend(domainLogic)
    new AtomicEventStoreFromBackend(eventStoreBackend, domainLogic.equalityRelations)
  }

  "A inMemory implementation of the event store " should behave like eventStoreWithAtomicProjection(UnitTest, inMemoryEventStoreWithProjection _, isolationLevelIsSerializable = false)

}

