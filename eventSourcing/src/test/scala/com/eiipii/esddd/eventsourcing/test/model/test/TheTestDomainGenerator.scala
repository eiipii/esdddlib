/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.test.model.test

import com.eiipii.esddd.test.DomainCoreCommandGenerator
import com.eiipii.esddd.test.model._
import org.scalacheck._

class TheTestDomainGenerator extends DomainCoreCommandGenerator[TheTestDomainModel, TestStateInMemory] {

  override def genCommand(state: TheTestState, projection: TestStateInMemory): Gen[TheTestCommand] = oneOrTwo

  lazy val oneOrTwo = Gen.oneOf(genOne, genTwo, genThreadCommand)

  lazy val genOne = Gen.const(TestCommandOne())
  lazy val genTwo = for {
    bool <- Gen.oneOf(true, false)
  } yield TestCommandTwo(bool)

  //The use of this commands will generate
  val maxAggregateNr = 20
  val maxConstraintNr = 20
  lazy val genThreadCommand = for {
    threadNr <- Gen.choose(0, 5)
    aggregateNr <- Gen.choose(0, maxAggregateNr)
    constraintNr <- Gen.choose(0, maxConstraintNr)
  } yield TestCommandForThreads(threadNr, aggregateNr, constraintNr)

}
