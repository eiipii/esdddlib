/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.api

import com.eiipii.esddd.eventsourcing.{EventStoreRange, EventStoreVersion}
import com.eiipii.esddd.eventsourcing.config.ESDDDConfiguration
import com.eiipii.esddd.eventsourcing.eventstore._
import com.eiipii.esddd.eventsourcing.logic.{DomainCoreLogic, FinalCommandExecutionResult}
import com.eiipii.esddd.eventsourcing.model.DomainErrorCode
import com.eiipii.esddd.test.Mocked
import com.eiipii.esddd.test.model._
import org.scalatest._

import scalaz._

class CommandExecutorOnEventStoreTest extends FlatSpec with Matchers {
  val logic = new TheTestDomainLogic()
  val testMaximumNumberOfRollbacks = 4

  it should "Stop transaction when the error is EventSourceCommandTransactionCalculationError" in {
    val anyValidErrorCode = -102
    val results = (0 to testMaximumNumberOfRollbacks + 5).map { nrOfCallsBeforeSuccess =>
      testWithNrOfErrors(nrOfCallsBeforeSuccess, EventSourceCommandTransactionCalculationError(DomainErrorCode(anyValidErrorCode)))
    }.toList

    results should be(List(
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode)))
    ))
  }

  it should "Stop transaction when the error is EventSourceCommandFailed" in {
    val anyValidErrorCode = -103
    val results = (0 to testMaximumNumberOfRollbacks + 5).map { nrOfCallsBeforeSuccess =>
      testWithNrOfErrors(nrOfCallsBeforeSuccess, EventSourceCommandFailed[TheTestDomainModel](DomainErrorCode(anyValidErrorCode), Map(), Map()))
    }.toList

    results should be(List(
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode))),
      -\/(CommandDomainError(DomainErrorCode(anyValidErrorCode)))
    ))
  }

  it should "Retry testMaximumNumberOfRollbacks before failing with InternalModelError when the error is EventSourceCommandScopeDeadLock" in {
    val results = (0 to testMaximumNumberOfRollbacks + 5).map { nrOfCallsBeforeSuccess =>
      testWithNrOfErrors(nrOfCallsBeforeSuccess, EventSourceCommandScopeDeadLock(new IllegalStateException()))
    }.toList

    results should be(List(
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached))
    ))
  }

  it should "Retry testMaximumNumberOfRollbacks before failing with InternalModelError when the error is EventSourceCommandRollback" in {
    val results = (0 to testMaximumNumberOfRollbacks + 5).map { nrOfCallsBeforeSuccess =>
      testWithNrOfErrors(nrOfCallsBeforeSuccess, EventSourceCommandRollback[TheTestDomainModel](Map(), Map()))
    }.toList

    results should be(List(
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached)),
      -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached))
    ))
  }

  it should "Stop transaction when the error is EventSourceCommandInternalError" in {
    val exception = new IllegalStateException()
    val results = (0 to testMaximumNumberOfRollbacks + 5).map { nrOfCallsBeforeSuccess =>
      testWithNrOfErrors(nrOfCallsBeforeSuccess, EventSourceCommandInternalError(exception))
    }.toList

    results should be(List(
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), List())),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception)),
      -\/(InternalExecutionError(FinalDomainFailure.internalException, exception))
    ))
  }

  it should "Pass domain errors" in {
    //given
    val eventStore: EventStore[TheTestDomainModel] = new TestEventStore(0, EventSourceCommandRollback[TheTestDomainModel](Map(), Map()))
    val config = ESDDDConfiguration(testMaximumNumberOfRollbacks)
    val executor = new CommandExecutorOnEventStore[TheTestDomainModel](logic, eventStore, config)
    val errorThreadNr = -121311

    //when a command creates a error on the domain logic
    val result = executor.execute(TestCommandForThreads(errorThreadNr, 0, 0))

    //then the error from the domain logic is extracted as the result of the execution
    result should be(-\/(CommandDomainError(DomainErrorCode(errorThreadNr))))
  }

  def testWithNrOfErrors(nrOfCallsBeforeSuccess: Int, errorValue: EventSourceCommandFailure): FinalCommandToEventsResult[TheTestDomainModel] = {
    val eventStore: EventStore[TheTestDomainModel] = new TestEventStore(nrOfCallsBeforeSuccess, errorValue)
    val config = ESDDDConfiguration(testMaximumNumberOfRollbacks)
    val executor = new CommandExecutorOnEventStore[TheTestDomainModel](logic, eventStore, config)
    executor.execute(TestCommandOne())
  }
}

private class TestEventStore(val nrOfCallsBeforeSuccess: Int, val errorValue: EventSourceCommandFailure) extends EventStore[TheTestDomainModel] {

  private var nrOfCalls = 0

  override def persistEvents(
    events:                 List[TheTestEvent],
    rootAggregate:          TheTestAggregate,
    atomicTransactionScope: AtomicTransactionScope[TheTestDomainModel]
  ): DomainCommandResult[TheTestDomainModel] = {
    nrOfCalls = nrOfCalls + 1
    if (nrOfCalls <= nrOfCallsBeforeSuccess) {
      -\/(errorValue)
    }
    else {
      \/-(FinalCommandExecutionResult[TheTestDomainModel](TestAggregateOne(), TestStateInMemory(), Seq()))
    }
  }

  override def calculateAtomicTransactionScopeVersion(
    logic:   DomainCoreLogic[TheTestDomainModel],
    command: TheTestCommand
  ): CommandToAtomicState[TheTestDomainModel] =
    \/-(AtomicTransactionScope[TheTestDomainModel](Map(), Map(), TestStateInMemory()))

  override def loadEvents(range: EventStoreRange): Seq[TheTestEvent] = Mocked.shouldNotBeCalled

  override def loadEventsForAggregate(aggregate: TheTestAggregate, range: EventStoreRange, nrOfEvents: Int): AggregateEventRangeView[TheTestDomainModel] = Mocked.shouldNotBeCalled

  override def registerUpdateListener(listener: EventStoreUpdateListener): EventStoreListenerCancellation = Mocked.shouldNotBeCalled

  override def registerAggregateListener(aggregate: TheTestAggregate, listener: EventStoreUpdateListener): EventStoreListenerCancellation = Mocked.shouldNotBeCalled

  override def loadCurrentVersion(): EventStoreVersion = Mocked.shouldNotBeCalled
}