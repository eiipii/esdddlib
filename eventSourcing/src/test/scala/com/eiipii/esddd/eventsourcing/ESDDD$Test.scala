/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing

import com.eiipii.esddd.eventsourcing.api.DomainInstance
import com.eiipii.esddd.eventsourcing.config.ESDDDConfiguration
import com.eiipii.esddd.eventsourcing.eventstore.inmemory.InMemoryEventStoreBackend
import com.eiipii.esddd.test.model.{TestCommandOne, TheTestDomainLogic, TheTestDomainModel}
import org.scalatest.{FlatSpec, Matchers}
import org.typelevel.scalatest.DisjunctionMatchers

class ESDDD$Test extends FlatSpec with Matchers with DisjunctionMatchers {

  it should "provide API to setup the event store domains" in {

    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withConfiguration(
        ESDDDConfiguration(maximumNumberOfRollbacks = 3)
      )
      .withBackend(InMemoryEventStoreBackend)
      .build()

    domain.commandExecution.execute(TestCommandOne()) shouldBe \/-
  }
}
