/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore

import com.eiipii.esddd.eventsourcing.eventstore.inmemory.{EventStoreInMemoryAtomicProjection, InMemoryEventStoreBackend}
import com.eiipii.esddd.eventsourcing.logic.{DomainCommandToAggregateScope, DomainCommandToConstraints}
import com.eiipii.esddd.eventsourcing.model.{DomainErrorCode, DomainFailure}
import com.eiipii.esddd.test.model._
import org.scalatest.{FlatSpec, Matchers}

import scalaz.-\/

class AtomicEventStoreFromBackendTest extends FlatSpec with Matchers {

  it should "Map domain aggregate calculation errors to EventSourceCommandTransactionCalculationError" in {
    //given
    val anyErrorCode: Long = -10101

    val domainLogicReturningErrors = new TheTestDomainLogic {
      override def calculateAggregates(command: TheTestCommand, state: TheTestState): DomainCommandToAggregateScope[TheTestDomainModel] =
        -\/(DomainFailure(DomainErrorCode(anyErrorCode)))
    }

    val backend = InMemoryEventStoreBackend.buildEventStoreBackend(domainLogicReturningErrors)

    val eventStore = new AtomicEventStoreFromBackend(backend, domainLogicReturningErrors.equalityRelations)

    //when
    val result = eventStore.calculateAtomicTransactionScopeVersion(domainLogicReturningErrors, TestCommandOne())

    //then
    result should be(-\/(EventSourceCommandTransactionCalculationError(DomainErrorCode(anyErrorCode))))
  }
  it should "Map domain constraint calculation errors to EventSourceCommandTransactionCalculationError" in {
    //given
    val anyErrorCode: Long = -20201

    val domainLogicReturningErrors = new TheTestDomainLogic {
      override def calculateConstraints(command: TheTestCommand, state: TheTestState): DomainCommandToConstraints[TheTestDomainModel] =
        -\/(DomainFailure(DomainErrorCode(anyErrorCode)))
    }

    val backend = InMemoryEventStoreBackend.buildEventStoreBackend(domainLogicReturningErrors)

    val eventStore = new AtomicEventStoreFromBackend(backend, domainLogicReturningErrors.equalityRelations)

    //when
    val result = eventStore.calculateAtomicTransactionScopeVersion(domainLogicReturningErrors, TestCommandOne())

    //then
    result should be(-\/(EventSourceCommandTransactionCalculationError(DomainErrorCode(anyErrorCode))))
  }
}
