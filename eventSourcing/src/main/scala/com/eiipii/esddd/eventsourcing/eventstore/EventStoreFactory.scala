/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore

import com.eiipii.esddd.eventsourcing.internal.atomic.Atomic
import com.eiipii.esddd.eventsourcing.logic.DomainCoreLogic
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}
import com.eiipii.esddd.eventsourcing.{AggregateReference, EventStoreRange, EventStoreVersion}

import scalaz.{-\/, \/-}

private[eventsourcing] final class AtomicEventStoreFromBackend[D <: DomainCoreModel](
    val eventStoreBackend: EventStoreTransactionalBackend[D],
    val equalityRelations: DomainEqualityRelations[D]
) extends EventStore[D] with EventStoreUpdatesListenerRegistry[D] {

  override def persistEvents(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): DomainCommandResult[D] =
    eventStoreBackend.persistEventsOnAtomicTransaction(events, rootAggregate, atomicTransactionScope) match {
      case error @ -\/(a) => error
      case ok @ \/-(b) => {
        triggerNewVersionAvailable(rootAggregate)
        ok
      }
    }

  override def calculateAtomicTransactionScopeVersion(logic: DomainCoreLogic[D], command: D#Command): CommandToAtomicState[D] = eventStoreBackend readOnly { transaction =>
    val projectionState: D#State = transaction.projectionState
    //logic calculations
    val scope = for {
      rootAggregate <- logic.calculateAggregates(command, projectionState)
      constraintsScope <- logic.calculateConstraints(command, projectionState)
    } yield (rootAggregate, constraintsScope)
    scope match {
      case domainError @ -\/(a) =>
        domainError.leftMap(domainFailure => EventSourceCommandTransactionCalculationError(domainFailure.error))
      case \/-(b) =>
        val (rootAggregate, constraintsScope) = b
        //backend access
        for {
          aggregateVersion <- transaction.calculateAggregateVersions(rootAggregate)
          constraintsVersions <- transaction.calculateConstraintVersions(constraintsScope)
        } yield AtomicTransactionScope[D](aggregateVersion, constraintsVersions, projectionState)
    }
  }

  override def loadEvents(range: EventStoreRange): Seq[D#Event] = eventStoreBackend readOnly { transaction =>
    transaction.extractEventRange(range)
  }

  override def loadEventsForAggregate(aggregate: D#Aggregate, range: EventStoreRange, nrOfEvents: Int): AggregateEventRangeView[D] = eventStoreBackend readOnly { transaction =>
    transaction.loadEventsForAggregate(aggregate, range, nrOfEvents)
  }

  override def loadCurrentVersion(): EventStoreVersion = eventStoreBackend readOnly { transaction =>
    transaction.calculateEventStoreVersion()
  }
}

private[eventsourcing] trait EventStoreUpdatesListenerRegistry[D <: DomainCoreModel] {

  def equalityRelations: DomainEqualityRelations[D]

  private val registry: Atomic[EventStoreListenersRegistry] = Atomic(EventStoreListenersRegistry())

  def registerUpdateListener(listener: EventStoreUpdateListener): EventStoreListenerCancellation = {
    val userSlot: Int = registry.updateAndGet { status =>
      status.copy(allEventsListeners = status.allEventsListeners.registerListener(listener))
    }.allEventsListeners.availableSlot - 1
    () =>
      registry.updateAndGet { status =>
        status.copy(allEventsListeners = status.allEventsListeners.unRegisterListener(userSlot))
      }
  }

  def registerAggregateListener(aggregate: D#Aggregate, listener: EventStoreUpdateListener): EventStoreListenerCancellation = {
    val aggregateReference: AggregateReference = equalityRelations.buildAggregateReference(aggregate)
    val usedSlot: Int = registry.updateAndGet { status =>
      val aggRegistryCurrent = status.perAggregate.getOrElse(aggregateReference, OrderedListenersRegistry())
      val aggRegistryUpdated = aggRegistryCurrent.registerListener(listener)
      status.copy(perAggregate = status.perAggregate.updated(aggregateReference, aggRegistryUpdated))
    }.perAggregate(aggregateReference).availableSlot - 1
    () =>
      registry.updateAndGet { status =>
        val aggRegistryCurrent = status.perAggregate(aggregateReference)
        val aggRegistryUpdated = aggRegistryCurrent.unRegisterListener(usedSlot)
        status.copy(perAggregate = status.perAggregate.updated(aggregateReference, aggRegistryUpdated))
      }
  }

  def triggerNewVersionAvailable(aggregate: D#Aggregate): Unit = {
    val aggregateReference: AggregateReference = equalityRelations.buildAggregateReference(aggregate)
    val references = registry().allEventsListeners.references
    references.values.foreach { l => l.onEventStoreVersionChange() }

    val forAggregate: Option[OrderedListenersRegistry] = registry().perAggregate.get(aggregateReference)
    forAggregate match {
      case Some(listenersRegistry) => listenersRegistry.references.values.foreach { l => l.onEventStoreVersionChange() }
      case None                    =>
    }
  }
}

private[eventsourcing] case class EventStoreListenersRegistry(
  allEventsListeners: OrderedListenersRegistry                          = OrderedListenersRegistry(),
  perAggregate:       Map[AggregateReference, OrderedListenersRegistry] = Map()
)

private[eventsourcing] case class OrderedListenersRegistry(references: Map[Int, EventStoreUpdateListener] = Map(), availableSlot: Int = 0) {

  def registerListener(listener: EventStoreUpdateListener): OrderedListenersRegistry =
    OrderedListenersRegistry(this.references.updated(availableSlot, listener), availableSlot + 1)

  def unRegisterListener(slot: Int): OrderedListenersRegistry = this.copy(references = this.references - slot)
}
