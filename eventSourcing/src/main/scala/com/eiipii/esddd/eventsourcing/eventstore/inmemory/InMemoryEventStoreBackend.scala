/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore.inmemory

import com.eiipii.esddd.eventsourcing._
import com.eiipii.esddd.eventsourcing.eventstore._
import com.eiipii.esddd.eventsourcing.internal.atomic.Atomic
import com.eiipii.esddd.eventsourcing.logic.{DomainCoreLogic, EventStoreAtomicProjectionPoint, FinalCommandExecutionResult}
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}

import scala.collection.immutable.Seq
import scalaz._

object InMemoryEventStoreBackend extends EventStoreBackend {

  def instance: InMemoryEventStoreBackend.type = this

  override def buildEventStoreBackend[D <: DomainCoreModel](domainLogic: DomainCoreLogic[D]): EventStoreTransactionalBackend[D] =
    new EventStoreInMemoryAtomicProjection[D](domainLogic.atomicProjectionProjection.zero, domainLogic.equalityRelations)
}

private[eventsourcing] class EventStoreInMemoryAtomicProjection[D <: DomainCoreModel](
    val zero:              EventStoreAtomicProjectionPoint[D],
    val equalityRelations: DomainEqualityRelations[D]
) extends EventStoreTransactionalBackend[D] with PersistEventCommonLogic[D] {

  private[this] val inMemoryStore = Atomic(AtomicEventStoreReadStateInMemory[D](zero))

  override def readOnly[A](execution: (EventStoreReadTransaction[D]) => A): A = execution(new InMemoryAtomicEventStoreReadTransaction(inMemoryStore(), equalityRelations))

  def persistEventsOnAtomicTransaction(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): DomainCommandResult[D] =
    internalPersistEventsOnAtomicTransaction(createPersistData(events, rootAggregate, atomicTransactionScope))

  def internalPersistEventsOnAtomicTransaction(data: EventPersistOperationData[D]): DomainCommandResult[D] = {
      def checkIfStateMatchTransactionScopeVersion(state: AtomicEventStoreReadStateInMemory[D]): EventSourceCommandFailure \/ AtomicEventStoreReadStateInMemory[D] = {
        val constraintsOk = data.constraints.find({
          case (constraintRef, constraintVersion) => state.constraintsVersion.getOrElse(constraintRef, 0L).compareTo(constraintVersion) != 0
        }) match {
          case Some(notMatchingConstraintVersion) => false
          case None                               => true
        }
        val aggregatesOk = data.aggregates.find({
          case (aggregateRef, aggregateVersion) => state.aggregatesVersion.getOrElse(aggregateRef, 0L).compareTo(aggregateVersion) != 0
        }) match {
          case Some(notMatchingAggregateVersion) => false
          case None                              => true
        }
        if (constraintsOk && aggregatesOk) {
          \/-(state)
        }
        else {
          -\/(EventSourceCommandRollback(data.atomicTransactionScope.aggregateVersion, data.atomicTransactionScope.constraintScopeVersion))
        }
      }
    val eventsOfAggregate: List[EventOfAggregate[D]] = data.events.map(EventOfAggregate(_, data.rootAggregate))

      def updateState(state: AtomicEventStoreReadStateInMemory[D]): AtomicEventStoreReadStateInMemory[D] = {
        val updatedEventHistory = state.eventHistoryList ++ eventsOfAggregate
        val updatedStateProjection = (state.groupElement /: data.events) {
          (groupElement, event) => groupElement.projectEvent(event)
        }
        val updatedAggregatesVersion = state.aggregatesVersion ++ (data.aggregates mapValues (_ + 1))
        val updatedConstraintsVersion = state.constraintsVersion ++ (data.constraints mapValues (_ + 1))
        val updatedVersion = state.version.add(data.events.size)
        AtomicEventStoreReadStateInMemory(updatedStateProjection, updatedEventHistory, updatedVersion, updatedAggregatesVersion, updatedConstraintsVersion)
      }

    inMemoryStore.updateAndGetWithCondition(updateState, checkIfStateMatchTransactionScopeVersion).map { postInternalState =>
      FinalCommandExecutionResult(data.rootAggregate, postInternalState.groupElement.state, data.events)
    }
  }
}

private[eventsourcing] class InMemoryAtomicEventStoreReadTransaction[D <: DomainCoreModel](
    val inMemoryState:     AtomicEventStoreReadStateInMemory[D],
    val equalityRelations: DomainEqualityRelations[D]
) extends EventStoreReadTransaction[D] {

  override def calculateConstraintVersions(constraints: Set[D#ConstraintScope]): EventSourceCommandFailure \/ Map[D#ConstraintScope, Long] = {
    val result: Map[D#ConstraintScope, Long] = constraints.map { constraint =>
      constraint -> inMemoryState.constraintsVersion.getOrElse(equalityRelations.buildConstraintReference(constraint).get, 0L)
    }(collection.breakOut)
    \/-(result)
  }

  override def calculateAggregateVersions(aggregate: Set[D#Aggregate]): EventSourceCommandFailure \/ Map[D#Aggregate, Long] = {
    val result: Map[D#Aggregate, Long] = aggregate.map { agg =>
      agg -> inMemoryState.aggregatesVersion.getOrElse(equalityRelations.buildAggregateReference(agg), 0L)
    }(collection.breakOut)
    \/-(result)
  }

  override def projectionState: D#State = inMemoryState.groupElement.state

  override def loadEventsForAggregate(aggregate: D#Aggregate, range: EventStoreRange, nrOfEvents: Int): AggregateEventRangeView[D] = {
    val firstEventVersion: EventStoreVersion = if (range.from.storeVersion == 0) { EventStoreVersion.zero.add(1) } else { range.from }
    val allEventsOnRange = EventStoreRangeUtils.extractRangeFromVector(inMemoryState.eventHistoryList, range)
    val allEventsOfTheAggregate = allEventsOnRange.zipWithIndex
      .filter {
        case (eventAgg, _) => eventAgg.aggregate == aggregate
      }
    val extracted: Vector[EventReference[D]] = allEventsOfTheAggregate
      .take(nrOfEvents)
      .map {
        case (eventAgg, index) => EventReference[D](eventAgg.event, firstEventVersion.add(index))
      }
    AggregateEventRangeView[D](extracted, allEventsOfTheAggregate.size == extracted.size)
  }

  override def extractEventRange(range: EventStoreRange): Seq[D#Event] =
    EventStoreRangeUtils.extractRangeFromVector(inMemoryState.eventHistoryList, range).map(_.event)

  override def calculateEventStoreVersion(): EventStoreVersion = inMemoryState.version
}

private[eventsourcing] case class AtomicEventStoreReadStateInMemory[D <: DomainCoreModel](
  groupElement:       EventStoreAtomicProjectionPoint[D],
  eventHistoryList:   Vector[EventOfAggregate[D]]        = Vector[EventOfAggregate[D]](),
  version:            EventStoreVersion                  = EventStoreVersion.zero,
  aggregatesVersion:  Map[AggregateReference, Long]      = Map[AggregateReference, Long](),
  constraintsVersion: Map[ConstraintReference, Long]     = Map[ConstraintReference, Long]()
)

private[eventsourcing] case class EventOfAggregate[D <: DomainCoreModel](event: D#Event, aggregate: D#Aggregate)
