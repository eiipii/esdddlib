/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore

import com.eiipii.esddd.eventsourcing.{AggregateReference, ConstraintReference, EventStoreRange, EventStoreVersion}
import com.eiipii.esddd.eventsourcing.logic.DomainCoreLogic
import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}

import scalaz.\/

private[eventsourcing] trait EventStoreBackend {

  def buildEventStoreBackend[D <: DomainCoreModel](domainLogic: DomainCoreLogic[D]): EventStoreTransactionalBackend[D]

}

private[eventsourcing] trait EventStoreTransactionalBackend[D <: DomainCoreModel] {

  def readOnly[A](execution: EventStoreReadTransaction[D] => A): A

  def persistEventsOnAtomicTransaction(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): DomainCommandResult[D]
}

private[eventsourcing] trait EventStoreReadTransaction[D <: DomainCoreModel] {
  def calculateEventStoreVersion(): EventStoreVersion

  def calculateConstraintVersions(constraints: Set[D#ConstraintScope]): EventSourceCommandFailure \/ Map[D#ConstraintScope, Long]

  def calculateAggregateVersions(aggregate: Set[D#Aggregate]): EventSourceCommandFailure \/ Map[D#Aggregate, Long]

  def projectionState: D#State

  def loadEventsForAggregate(aggregate: D#Aggregate, range: EventStoreRange, nrOfEvents: Int): AggregateEventRangeView[D]

  def extractEventRange(range: EventStoreRange): Seq[D#Event]

}

private[eventsourcing] trait PersistEventCommonLogic[D <: DomainCoreModel] {

  def equalityRelations: DomainEqualityRelations[D]

  def createPersistData(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): EventPersistOperationData[D] = {
    val rootAggregateRef: AggregateReference = equalityRelations.buildAggregateReference(rootAggregate)
    val constraintsReferenceVersion: Map[ConstraintReference, Long] = atomicTransactionScope.constraintScopeVersion.map {
      case (constraint, version) => (equalityRelations.buildConstraintReference(constraint).get, version)
    }
    val aggregatesReferenceVersion = atomicTransactionScope.aggregateVersion.map {
      case (aggregate, version) => (equalityRelations.buildAggregateReference(aggregate), version)
    }
    val rootAggregateVersion = aggregatesReferenceVersion.get(rootAggregateRef) match {
      case Some(rootVersion) => rootVersion
      case None              => 0L
    }
    val mergedTransactionAggregatesReferences: Map[AggregateReference, Long] = aggregatesReferenceVersion.get(rootAggregateRef) match {
      case Some(rootVersion) => aggregatesReferenceVersion
      case None              => aggregatesReferenceVersion ++ Map(rootAggregateRef -> rootAggregateVersion)
    }
    EventPersistOperationData(events, rootAggregate, atomicTransactionScope, rootAggregateRef, rootAggregateVersion, mergedTransactionAggregatesReferences, constraintsReferenceVersion)
  }

}

private[eventsourcing] case class EventPersistOperationData[D <: DomainCoreModel](
  events:                 List[D#Event],
  rootAggregate:          D#Aggregate,
  atomicTransactionScope: AtomicTransactionScope[D],
  rootAggregateRef:       AggregateReference,
  rootAggregateVersion:   Long,
  aggregates:             Map[AggregateReference, Long],
  constraints:            Map[ConstraintReference, Long]
)