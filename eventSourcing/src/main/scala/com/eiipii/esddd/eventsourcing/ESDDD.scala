/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing

import com.eiipii.esddd.eventsourcing.api.{CommandExecutorOnEventStore, DomainInstance, EventCommandExecution}
import com.eiipii.esddd.eventsourcing.config.ESDDDConfiguration
import com.eiipii.esddd.eventsourcing.eventstore.inmemory.InMemoryEventStoreBackend
import com.eiipii.esddd.eventsourcing.eventstore.{AtomicEventStoreFromBackend, EventStoreBackend, EventStoreInteraction}
import com.eiipii.esddd.eventsourcing.logic.DomainCoreLogic
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel

object ESDDD {

  val domain = DomainBase()

  object Domain {

  }

  case class DomainBase() {
    def withDomainLogic[D <: DomainCoreModel](logic: DomainCoreLogic[D]): DomainDefinition[D] = DomainDefinition(logic)
  }

  case class DomainDefinition[D <: DomainCoreModel](
      logic:           DomainCoreLogic[D],
      configuration:   ESDDDConfiguration        = ESDDDConfiguration(),
      backendProvider: Option[EventStoreBackend] = None
  ) {

    def withConfiguration(value: ESDDDConfiguration): DomainDefinition[D] = this.copy(configuration = value)

    def withBackend(backend: EventStoreBackend): DomainReady[D] = DomainReady[D](this, backend)

  }

  case class DomainReady[D <: DomainCoreModel](
      definition:      DomainDefinition[D],
      backendProvider: EventStoreBackend
  ) {
    def build(): DomainInstance[D] = {
      val eventStoreInstance = new AtomicEventStoreFromBackend(
        backendProvider.buildEventStoreBackend(definition.logic),
        definition.logic.equalityRelations
      )
      val commandExecutorOnEventStore = new CommandExecutorOnEventStore(definition.logic, eventStoreInstance, ESDDDConfiguration(0))
      new DomainInstance[D] {
        override def commandExecution: EventCommandExecution[D] = commandExecutorOnEventStore

        override def eventStore: EventStoreInteraction[D] = eventStoreInstance
      }
    }
  }

}

trait DefaultInMemoryEventCommandExecutionForDomainLogic {

  def domainInstanceFromLogic[D <: DomainCoreModel](domainLogic: DomainCoreLogic[D]): DomainInstance[D] =
    ESDDD.domain
      .withDomainLogic(domainLogic)
      .withBackend(InMemoryEventStoreBackend)
      .build()

}