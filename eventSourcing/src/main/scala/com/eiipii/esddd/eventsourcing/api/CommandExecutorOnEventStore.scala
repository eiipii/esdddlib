/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.api

import com.eiipii.esddd.eventsourcing.config.ESDDDConfiguration
import com.eiipii.esddd.eventsourcing.eventstore._
import com.eiipii.esddd.eventsourcing.logic._
import com.eiipii.esddd.eventsourcing.model._
import com.typesafe.scalalogging.LazyLogging

import scalaz._

class CommandExecutorOnEventStore[D <: DomainCoreModel](
    val logic:      DomainCoreLogic[D],
    val eventStore: EventStore[D],
    val config:     ESDDDConfiguration
) extends EventCommandExecution[D] with LazyLogging {
  final override def execute(command: D#Command): FinalCommandToEventsResult[D] = {
      def singleTry(): DomainCommandResult[D] = {
        for {
          atomicTransactionScope <- calculateAtomicTransactionScope(command)
          eventsAndRoot <- applyCommand(command, atomicTransactionScope)
          confirmation <- eventStore.persistEvents(eventsAndRoot.events, eventsAndRoot.rootAggregate, atomicTransactionScope)
        } yield confirmation
      }

      def executionLoop(stackDeep: Int): FinalCommandToEventsResult[D] = {
          def continue(): FinalCommandToEventsResult[D] = {
            if (stackDeep > 0) {
              executionLoop(stackDeep - 1)
            }
            else {
              -\/(InternalModelError(FinalDomainFailure.maximumNumberOfRollbackRetriesReached))
            }
          }

        //TODO logging and monitoring
        singleTry() match {
          case r @ \/-(b) => r
          case -\/(a) =>
            a match {
              case EventSourceCommandTransactionCalculationError(error)                   => -\/(CommandDomainError(error))
              case EventSourceCommandFailed(error, constrainsVersions, aggregateVersions) => -\/(CommandDomainError(error))
              case EventSourceCommandScopeDeadLock(throwable)                             => continue()
              case EventSourceCommandRollback(constrainsVersions, aggregateVersions)      => continue()
              case EventSourceCommandInternalError(throwable)                             => -\/(InternalExecutionError(FinalDomainFailure.internalException, throwable))
            }
        }
      }
    logger.debug("Execution command {}.", command)
    executionLoop(config.maximumNumberOfRollbacks)
  }

  private def applyCommand(command: D#Command, atomicTransactionScope: AtomicTransactionScope[D]): CommandToEventsResult[D] =
    logic.calculateCommandToEvents(command)(atomicTransactionScope.projectionView).leftMap(domainFailure =>
      EventSourceCommandFailed(domainFailure.error, atomicTransactionScope.aggregateVersion, atomicTransactionScope.constraintScopeVersion))

  private def calculateAtomicTransactionScope(command: D#Command): CommandToAtomicState[D] =
    eventStore.calculateAtomicTransactionScopeVersion(logic, command)
}

