/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore

import com.eiipii.esddd.eventsourcing.{EventData, EventStoreRange, EventStoreVersion}
import com.eiipii.esddd.eventsourcing.logic.DomainCoreLogic
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel

trait EventStore[D <: DomainCoreModel] extends EventStoreInteraction[D] {

  def persistEvents(events: List[D#Event], rootAggregate: D#Aggregate, atomicTransactionScope: AtomicTransactionScope[D]): DomainCommandResult[D]

  def calculateAtomicTransactionScopeVersion(logic: DomainCoreLogic[D], command: D#Command): CommandToAtomicState[D]

}

trait EventStoreInteraction[D <: DomainCoreModel] {
  def loadCurrentVersion(): EventStoreVersion

  def loadEvents(range: EventStoreRange): Seq[D#Event]

  def loadEventsForAggregate(aggregate: D#Aggregate, range: EventStoreRange, nrOfEvents: Int): AggregateEventRangeView[D]

  def registerUpdateListener(listener: EventStoreUpdateListener): EventStoreListenerCancellation

  def registerAggregateListener(aggregate: D#Aggregate, listener: EventStoreUpdateListener): EventStoreListenerCancellation

}

/** A view of the range of events for a aggregate.
 *  @param events Ordered seq of events on the range
 *  @param allRangeExtracted true if all the events on the range has been extracted
 *  @tparam D domain model
 */
case class AggregateEventRangeView[D <: DomainCoreModel](events: Vector[EventReference[D]], allRangeExtracted: Boolean)
case class EventReference[D <: DomainCoreModel](event: D#Event, version: EventStoreVersion)

trait EventStoreUpdateListener {
  def onEventStoreVersionChange(): Unit
}
trait EventStoreListenerCancellation {
  def cancelListener(): Unit
}

case class AtomicTransactionScope[D <: DomainCoreModel](
  aggregateVersion:       Map[D#Aggregate, Long],
  constraintScopeVersion: Map[D#ConstraintScope, Long],
  projectionView:         D#State
)

trait EventSerializationSchema[D <: DomainCoreModel] {

  def mapToEvent(data: EventData): D#Event

  def eventToData(event: D#Event): EventData
}