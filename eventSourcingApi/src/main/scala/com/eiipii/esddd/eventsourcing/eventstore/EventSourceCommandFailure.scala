/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.eventstore

import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainErrorCode}

/** Define the possible failure after a command execution
 */
sealed trait EventSourceCommandFailure

/** Domain logic return a error during transaction calculation
 *
 *  @param error domain error code
 */
case class EventSourceCommandTransactionCalculationError(error: DomainErrorCode) extends EventSourceCommandFailure

/** A command failure that indicates that the command can not be executed
 *  and client must notified about unsuccessful execution of command.
 *
 *  @param error code that inform about the internal failure reason.
 */
case class EventSourceCommandFailed[D <: DomainCoreModel](
  error:                  DomainErrorCode,
  aggregateVersion:       Map[D#Aggregate, Long],
  constraintScopeVersion: Map[D#ConstraintScope, Long]
) extends EventSourceCommandFailure

/** Command execution has produced a transaction rollback because of concurrent command execution.
 *  Command can be re-executed in the new event store state without client notification.
 */
case class EventSourceCommandRollback[D <: DomainCoreModel](
  aggregateVersion:       Map[D#Aggregate, Long],
  constraintScopeVersion: Map[D#ConstraintScope, Long]
) extends EventSourceCommandFailure

/** Calculation of transaction scope has produced a dead lock on backend.
 *  Command can be re-executed in the new event store state without client notification.
 */
case class EventSourceCommandScopeDeadLock(throwable: Throwable) extends EventSourceCommandFailure

/** Command execution have produced a exception.
 *  Exception must be reported to calling client.
 */
case class EventSourceCommandInternalError(throwable: Throwable) extends EventSourceCommandFailure
