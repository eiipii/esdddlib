/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 *
 */

package com.eiipii.esddd.eventsourcing.logic

import com.eiipii.esddd.eventsourcing.model.{DomainCoreModel, DomainEqualityRelations}

trait DomainCoreLogic[D <: DomainCoreModel] {

  def calculateAggregates(command: D#Command, state: D#State): DomainCommandToAggregateScope[D]

  def calculateConstraints(command: D#Command, state: D#State): DomainCommandToConstraints[D]

  def calculateCommandToEvents(command: D#Command)(implicit state: D#State): DomainCommandToEventsResult[D]

  def atomicProjectionProjection: EventStoreAtomicProjectionPoint[D]

  def equalityRelations: DomainEqualityRelations[D]

}

trait EventStoreAtomicProjectionPoint[D <: DomainCoreModel] {
  def zero: EventStoreAtomicProjectionPoint[D]

  def state: D#State

  def projectEvent(event: D#Event): EventStoreAtomicProjectionPoint[D]
}

case class CommandExecutionResult[E, A](events: List[E], rootAggregate: A)

case class FinalCommandExecutionResult[D <: DomainCoreModel](rootAggregate: D#Aggregate, state: D#State, events: Seq[D#Event])

