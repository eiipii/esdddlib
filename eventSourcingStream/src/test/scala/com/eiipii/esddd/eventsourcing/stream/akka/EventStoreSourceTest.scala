/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package com.eiipii.esddd.eventsourcing.stream.akka

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import akka.stream.testkit.TestSubscriber.Probe
import akka.stream.testkit.scaladsl.TestSink
import akka.testkit.TestProbe
import com.eiipii.esddd.eventsourcing.api.DomainInstance
import com.eiipii.esddd.eventsourcing.eventstore.inmemory.InMemoryEventStoreBackend
import com.eiipii.esddd.eventsourcing.{ESDDD, EventStoreRange, EventStoreVersion}
import com.eiipii.esddd.test.model._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._

class EventStoreSourceTest extends FlatSpec with Matchers with ScalaFutures {
  implicit val system = ActorSystem("test")
  implicit val materializer = ActorMaterializer()

  it should "provide a akka source of events" in {
    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()

    val nrOfEventsToProject = 10
    val batchSize = 3

    val source: Source[TheTestEvent, NotUsed] = EventStoreSource.eventsSource(domain.eventStore, batchSize)

    val valuesF: Probe[TheTestEvent] = source.take(nrOfEventsToProject).runWith(TestSink.probe[TheTestEvent])

    valuesF.request(batchSize * 2)
    valuesF.expectNoMessage(200.millis)

    (1 to 13).foreach { _ =>
      domain.commandExecution.execute(TestCommandOne())
    }
    (1 to batchSize * 2).foreach(_ => valuesF.expectNext(TestEventOne()))

    valuesF.request(Long.MaxValue)
    val expected = nrOfEventsToProject - 2 * batchSize
    (1 to expected).foreach(_ => valuesF.expectNext(TestEventOne()))
    valuesF.expectComplete()

  }

  it should "provide a akka source for a selected aggregate" in {

    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()

    val aggregateSource: Source[TheTestEvent, NotUsed] = EventStoreSource.aggregateEventsSource[TheTestDomainModel](domain.eventStore, TestAggregateOne())

    val probe = TestProbe()
    aggregateSource.to(Sink.actorRef(probe.ref, "completed")).run()

    (1 to 13).foreach { _ =>
      domain.commandExecution.execute(TestCommandTwo(true))
    }
    domain.eventStore.loadEvents(EventStoreRange(EventStoreVersion.zero)) should have size 13
    //No events on stream
    probe.expectNoMessage(200.millis)

    (0 to 13).foreach { _ =>
      domain.commandExecution.execute(TestCommandOne())
    }
    probe.expectMsgAllClassOf(2.seconds, classOf[TestEventOne])

  }

  it should "provide a snapshot source for an aggregate" in {
    //Given
    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()

    //And
    val aggregateSource: Source[TheTestEvent, EventStoreVersion] = EventStoreSource.aggregateSnapshotSource[TheTestDomainModel](domain.eventStore, TestAggregateThread(1))

    //When commands are created
    val preSourceNrOfEvents = 131
    (1 to preSourceNrOfEvents).foreach { nr =>
      domain.commandExecution.execute(TestCommandForThreads(nr, 1, 0))
    }
    //And the stream is materialized
    val (version, probe) = aggregateSource.toMat(TestSink.probe[TheTestEvent])(Keep.both).run()
    val postSourceNrOfEvents = 43
    (1 to postSourceNrOfEvents).foreach { i =>
      domain.commandExecution.execute(TestCommandForThreads(preSourceNrOfEvents + i, 1, 0))
    }

    //Then
    version should be(EventStoreVersion(preSourceNrOfEvents))
    domain.eventStore.loadEvents(EventStoreRange(EventStoreVersion.zero)) should have size (postSourceNrOfEvents + preSourceNrOfEvents)

    probe.request(Long.MaxValue)
    (1 to preSourceNrOfEvents).foreach { nr =>
      probe.expectNext(TestEventThread(nr, 1, 0))
    }
    probe.expectComplete()

  }
}
