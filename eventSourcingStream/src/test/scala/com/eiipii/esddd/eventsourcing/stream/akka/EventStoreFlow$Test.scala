/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.stream.akka

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.FlowMonitorState.Received
import akka.stream.scaladsl._
import akka.stream.testkit.TestSubscriber.Probe
import akka.stream.testkit.scaladsl.TestSink
import akka.stream.{ActorMaterializer, FlowMonitor}
import com.eiipii.esddd.eventsourcing.{ESDDD, EventStoreVersion}
import com.eiipii.esddd.eventsourcing.api.DomainInstance
import com.eiipii.esddd.eventsourcing.eventstore.inmemory.InMemoryEventStoreBackend
import com.eiipii.esddd.test.model.{TestStateInMemory, TheTestEvent, _}
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

class EventStoreFlow$Test extends FlatSpec with Matchers with GivenWhenThen {

  implicit val system = ActorSystem("testProjections")
  implicit val materializer = ActorMaterializer()

  it should "create a stream of projection values with monitor" in {
    Given("A domain instance")
    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()
    And("a projection graph with monitor is created")
    val source: Source[TheTestEvent, NotUsed] = EventStoreSource.eventsSource(domain.eventStore)
    val projFlow: Flow[TheTestEvent, TestStateInMemory, FlowMonitor[TestStateInMemory]] = EventStoreFlow.projectionFlowWithMonitor(new TheEventStateProjection())
    val graph: Source[TestStateInMemory, FlowMonitor[TestStateInMemory]] = source.viaMat(projFlow)(Keep.right)

    When("The graph is running")
    val (monitor, probe) = graph.toMat(TestSink.probe[TestStateInMemory])(Keep.both).run()
    And("three ONE commands are run on the domain")
    (1 to 3).foreach { _ =>
      domain.commandExecution.execute(TestCommandOne())
    }

    Then("The stream provide the projections in orden")
    probe
      .request(4)
      .expectNext(TestStateInMemory())
      .expectNext(TestStateInMemory(1, 0, 0, 1, "1,"))
      .expectNext(TestStateInMemory(2, 0, 0, 1, "1,1,"))
      .expectNext(TestStateInMemory(3, 0, 0, 1, "1,1,1,"))

    And("The monitor points to the correct value")
    monitor.state match {
      case Received(msg) => msg should be(TestStateInMemory(3, 0, 0, 1, "1,1,1,"))
      case _             => fail(s"monitor do not point to the last projection value, monitor: '${monitor}'")
    }
  }

  it should "follow the value of the stream with the monitor" in {
    Given("A domain instance")
    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()
    And("a projection graph is created")
    val source: Source[TheTestEvent, NotUsed] = EventStoreSource.eventsSource(domain.eventStore)
    val projFlow: Flow[TheTestEvent, TestStateInMemory, FlowMonitor[TestStateInMemory]] = EventStoreFlow.projectionFlowWithMonitor(new TheEventStateProjection())
    val graph: Source[TestStateInMemory, FlowMonitor[TestStateInMemory]] = source.viaMat(projFlow)(Keep.right)

    When("The graph is running")
    val (monitor, probe) = graph.toMat(TestSink.probe[TestStateInMemory])(Keep.both).run()

    Then("The first stream element is the zero projection")
    probe
      .request(1)
      .expectNext(TestStateInMemory())

    And("The monitor points to the correct value")
    monitor.state match {
      case Received(msg) => msg should be(TestStateInMemory())
      case _             => fail(s"monitor do not point to the last projection value, monitor: '${monitor}'")
    }
  }

  it should "create a snapshot projection based on a event store version" in {
    Given("A domain instance")
    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()
    And("a snapshot projection is created")
    val aggregateSource: Source[TheTestEvent, EventStoreVersion] = EventStoreSource.aggregateSnapshotSource[TheTestDomainModel](domain.eventStore, TestAggregateOne())
    val projFlow: Flow[TheTestEvent, TestStateInMemory, NotUsed] = EventStoreFlow.projectionFlow(new TheEventStateProjection())
    val graph: Source[TestStateInMemory, EventStoreVersion] = aggregateSource.via(projFlow)
    And("three command are executed on the domain BEFORE the stream execution")
    (1 to 3).foreach { _ =>
      domain.commandExecution.execute(TestCommandOne())
    }

    When("The graph is running")
    val probe: Probe[TestStateInMemory] = graph.runWith(TestSink.probe[TestStateInMemory])
    And("three command are executed on the domain AFTER the stream execution")
    (1 to 3).foreach { _ =>
      domain.commandExecution.execute(TestCommandOne())
    }

    Then("The stream should get")
    val moreThanNeeded = 10
    probe.request(moreThanNeeded)
      .expectNext(TestStateInMemory())
      .expectNext(TestStateInMemory(1, 0, 0, 1, "1,"))
      .expectNext(TestStateInMemory(2, 0, 0, 1, "1,1,"))
      .expectNext(TestStateInMemory(3, 0, 0, 1, "1,1,1,"))
      .expectComplete()
  }
}
