/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.stream.akka.internal

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Source}
import akka.stream.testkit.TestSubscriber.Probe
import akka.stream.testkit.scaladsl.TestSink
import com.eiipii.esddd.eventsourcing.api.DomainInstance
import com.eiipii.esddd.eventsourcing.eventstore.inmemory.InMemoryEventStoreBackend
import com.eiipii.esddd.eventsourcing.stream.akka.{EventStoreFlow, EventStoreSource}
import com.eiipii.esddd.eventsourcing.{ESDDD, EventStoreVersion}
import com.eiipii.esddd.test.model._
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

class EventStoreSnapshotSourceTest extends FlatSpec with Matchers with GivenWhenThen {

  implicit val system = ActorSystem("testFinishBeNoMoreToLoad")
  implicit val materializer = ActorMaterializer()

  it should "store the stream when there is no more values to read" in {
    Given("A domain instance")
    val domain: DomainInstance[TheTestDomainModel] = ESDDD.domain
      .withDomainLogic(new TheTestDomainLogic())
      .withBackend(InMemoryEventStoreBackend)
      .build()
    And("a snapshot projection is created that will load one event on each batch")
    val batchSize = 1
    val aggregateSource: Source[TheTestEvent, EventStoreVersion] = EventStoreSource.aggregateSnapshotSource[TheTestDomainModel](domain.eventStore, TestAggregateOne(), batchSize)
    val projFlow: Flow[TheTestEvent, TestStateInMemory, NotUsed] = EventStoreFlow.projectionFlow(new TheEventStateProjection())
    val graph: Source[TestStateInMemory, EventStoreVersion] = aggregateSource.via(projFlow)
    And("The event store do not is empty BEFORE the stream creation")

    When("The graph is running")
    val probe: Probe[TestStateInMemory] = graph.runWith(TestSink.probe[TestStateInMemory])
    And("three command are executed on the domain AFTER the stream execution")
    (1 to 3).foreach { _ =>
      domain.commandExecution.execute(TestCommandOne())
      domain.commandExecution.execute(TestCommandTwo(true))
    }

    Then("The stream should get")
    val moreThanNeeded = 10
    probe.request(moreThanNeeded)
      .expectNext(TestStateInMemory())
      .expectComplete()
    And("The coverage data should show that the range extraction logic returned None and close the stream")
  }
}
