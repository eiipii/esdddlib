/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.stream.akka

import akka.NotUsed
import akka.stream.SourceShape
import akka.stream.scaladsl.Source
import akka.stream.stage.GraphStageLogic
import com.eiipii.esddd.eventsourcing.EventStoreVersion
import com.eiipii.esddd.eventsourcing.eventstore.EventStoreInteraction
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel
import com.eiipii.esddd.eventsourcing.stream.akka.internal._

object EventStoreSource {
  val defaultEventBatchSize = 10

  def eventsSource[D <: DomainCoreModel](eventStore: EventStoreInteraction[D], batchSize: Int = defaultEventBatchSize): Source[D#Event, NotUsed] =
    Source.fromGraph(new EventStoreSourceStage[D, NotUsed]() {
      override def createEventStoreLogic(shape: SourceShape[D#Event]): (GraphStageLogic, NotUsed) =
        (new EventStoreAllEventsSource[D](shape, eventStore, batchSize), NotUsed)

    })

  def aggregateEventsSource[D <: DomainCoreModel](eventStore: EventStoreInteraction[D], aggregate: D#Aggregate, batchSize: Int = defaultEventBatchSize): Source[D#Event, NotUsed] =
    Source.fromGraph(new EventStoreSourceStage[D, NotUsed]() {
      override def createEventStoreLogic(shape: SourceShape[D#Event]): (GraphStageLogic, NotUsed) =
        (new EventStoreAggregateSource[D](shape, eventStore, aggregate, batchSize), NotUsed)

    })

  def aggregateSnapshotSource[D <: DomainCoreModel](eventStore: EventStoreInteraction[D], aggregate: D#Aggregate, batchSize: Int = defaultEventBatchSize): Source[D#Event, EventStoreVersion] =
    Source.fromGraph(new EventStoreSourceStage[D, EventStoreVersion]() {
      override def createEventStoreLogic(shape: SourceShape[D#Event]): (GraphStageLogic, EventStoreVersion) = {
        val logic = new EventStoreSnapshotSource[D](shape, eventStore, aggregate, batchSize)
        (logic, logic.snapshotVersion)
      }
    })
}

