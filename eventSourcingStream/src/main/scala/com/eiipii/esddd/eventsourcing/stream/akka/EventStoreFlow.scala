/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.stream.akka

import akka.NotUsed
import akka.stream.FlowMonitor
import akka.stream.scaladsl.{Flow, Keep}
import com.eiipii.esddd.eventsourcing.logic.EventGroupProjection
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel

object EventStoreFlow {

  def projectionFlow[D <: DomainCoreModel, P](projection: EventGroupProjection[D, P]): Flow[D#Event, P, NotUsed] =
    Flow[D#Event].scan(projection.zero) {
      (state, event) => projection.projectSingleEvent(state, event)
    }

  def projectionFlowWithMonitor[D <: DomainCoreModel, P](projection: EventGroupProjection[D, P]): Flow[D#Event, P, FlowMonitor[P]] =
    projectionFlow(projection).monitor()(Keep.right)

}
