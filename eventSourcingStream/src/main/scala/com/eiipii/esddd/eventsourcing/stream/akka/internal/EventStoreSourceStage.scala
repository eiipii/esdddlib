/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Paweł Cesar Sanjuan Szklarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.eiipii.esddd.eventsourcing.stream.akka.internal

import akka.stream.stage._
import akka.stream.{Attributes, Outlet, SourceShape}
import com.eiipii.esddd.eventsourcing.{EventStoreRange, EventStoreVersion}
import com.eiipii.esddd.eventsourcing.eventstore.{AggregateEventRangeView, EventStoreInteraction, EventStoreListenerCancellation}
import com.eiipii.esddd.eventsourcing.model.DomainCoreModel

import scala.annotation.tailrec

abstract class EventStoreSourceStage[D <: DomainCoreModel, M]() extends GraphStageWithMaterializedValue[SourceShape[D#Event], M] {
  private val out: Outlet[D#Event] = Outlet("EventStream")
  override val shape: SourceShape[D#Event] = SourceShape(out)

  def createEventStoreLogic(shape: SourceShape[D#Event]): (GraphStageLogic, M)

  @scala.throws[Exception](classOf[Exception])
  override def createLogicAndMaterializedValue(inheritedAttributes: Attributes): (GraphStageLogic, M) = createEventStoreLogic(shape)
}

private[akka] class EventStoreAllEventsSource[D <: DomainCoreModel](
    val shape:             SourceShape[D#Event],
    val eventStore:        EventStoreInteraction[D],
    val eventBatchMaxSize: Int
) extends GraphStageLogic(shape) with EventStoreSourceLogic[D] {

  private[this] var loadedEventVersion = EventStoreVersion.zero

  override def registerListener(eventUpdateCallback: AsyncCallback[Unit]): EventStoreListenerCancellation =
    eventStore.registerUpdateListener(() => eventUpdateCallback.invoke(()))

  override def loadMore(): Seq[D#Event] = {
    val loadEvents = eventStore.loadEvents(EventStoreRange(loadedEventVersion, Some(loadedEventVersion.add(eventBatchMaxSize))))
    loadedEventVersion = loadedEventVersion.add(loadEvents.size)
    loadEvents
  }
}

private[akka] class EventStoreAggregateSource[D <: DomainCoreModel](
    val shape:             SourceShape[D#Event],
    val eventStore:        EventStoreInteraction[D],
    val aggregate:         D#Aggregate,
    val eventBatchMaxSize: Int
) extends GraphStageLogic(shape) with EventStoreSourceLogic[D] with AggregateLoadingLogic[D] {

  override def calculateLoadRange(alreadyLoaded: EventStoreVersion): Option[EventStoreRange] = Some(EventStoreRange(alreadyLoaded.add(1)))

}

private[akka] class EventStoreSnapshotSource[D <: DomainCoreModel](
    val shape:             SourceShape[D#Event],
    val eventStore:        EventStoreInteraction[D],
    val aggregate:         D#Aggregate,
    val eventBatchMaxSize: Int
) extends GraphStageLogic(shape) with EventStoreSourceLogic[D] with AggregateLoadingLogic[D] {

  lazy val snapshotVersion: EventStoreVersion = eventStore.loadCurrentVersion()
  lazy val rangeToLoadEnd = snapshotVersion.add(1)

  override def calculateLoadRange(alreadyLoaded: EventStoreVersion): Option[EventStoreRange] =
    if (alreadyLoaded.storeVersion.compareTo(snapshotVersion.storeVersion) == 0) {
      None
    }
    else {
      Some(EventStoreRange(alreadyLoaded.add(1), Some(rangeToLoadEnd)))
    }

  override def onLoad(aggregateEventRangeView: AggregateEventRangeView[D]): Unit = if (aggregateEventRangeView.allRangeExtracted) {
    prepareForComplete()
  }
}

private[akka] trait AggregateLoadingLogic[D <: DomainCoreModel] {
  self: EventStoreSourceLogic[D] =>
  def aggregate: D#Aggregate

  def calculateLoadRange(alreadyLoaded: EventStoreVersion): Option[EventStoreRange]

  private[this] var loadedEventVersion = EventStoreVersion.zero

  def onLoad(aggregateEventRangeView: AggregateEventRangeView[D]): Unit = {}

  override def loadMore(): Seq[D#Event] = {
    val rangeOp = calculateLoadRange(loadedEventVersion)
    rangeOp match {
      case Some(range) => loadRange(range)
      case None =>
        prepareForComplete()
        Seq()
    }
  }

  def loadRange(range: EventStoreRange): Seq[D#Event] = {
    val loadEvents = eventStore.loadEventsForAggregate(aggregate, range, eventBatchMaxSize)
    if (loadEvents.events.nonEmpty) {
      loadedEventVersion = loadEvents.events.last.version
    }
    onLoad(loadEvents)
    loadEvents.events.map(_.event)
  }

  override def registerListener(eventUpdateCallback: AsyncCallback[Unit]): EventStoreListenerCancellation =
    eventStore.registerAggregateListener(aggregate, () => eventUpdateCallback.invoke(()))
}

private[akka] trait EventStoreSourceLogic[D <: DomainCoreModel] extends GraphStageLogic {

  def shape: SourceShape[D#Event]

  def eventStore: EventStoreInteraction[D]

  def eventBatchMaxSize: Int

  def loadMore(): Seq[D#Event]

  def prepareForComplete(): Unit = {
    readyForComplete = true
  }

  var buffer: Iterator[D#Event] = Iterator.empty
  var readyForComplete: Boolean = false
  var listenerCancellation: Option[EventStoreListenerCancellation] = None

  def registerListener(eventUpdateCallback: AsyncCallback[Unit]): EventStoreListenerCancellation

  override def preStart(): Unit = {
    val eventUpdateCallback = getAsyncCallback[Unit] { (_) =>
      pump()
    }
    listenerCancellation = Some(registerListener(eventUpdateCallback))
  }

  setHandler(shape.out, new OutHandler {
    override def onPull(): Unit = {
      pump()
    }
  })

  override def postStop(): Unit = {
    listenerCancellation.foreach(_.cancelListener())
  }

  @tailrec
  private def pump(): Unit = {
    if (isAvailable(shape.out)) {
      if (buffer.hasNext) {
        val event = buffer.next()
        push(shape.out, event)
        pump()
      }
      else {
        if (readyForComplete) {
          complete(shape.out)
        }
        else {
          val loaded = loadMore()
          if (loaded.nonEmpty) {
            buffer = buffer ++ loaded.iterator
            pump()
          }
          else if (readyForComplete) {
            complete(shape.out)
          }
        }
      }
    }
  }

}

